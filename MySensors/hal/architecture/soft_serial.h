#ifndef SOFT_SERIAL_H_
#define SOFT_SERIAL_H_
#ifdef __cplusplus
extern "C" {
#endif

#define SoftSerialPortDIR P2DIR
#define SoftSerialPortOUT P2OUT
#define SoftSerialPortIN P2IN

//serial_setup(softTXD, softRXD, ((16000000/1024)*1000) / 9600);
void serial_setup(unsigned out_mask, unsigned in_mask, unsigned bit_duration);
void putc(unsigned c);
//void puts(const unsigned char *s);
void puts(const char *_ptr);
unsigned char getc(void);

#ifdef __cplusplus
}
#endif
#endif /* SOFT_SERIAL_H_ */
