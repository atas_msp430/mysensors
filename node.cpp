
#include "MySensors/MySensors.h"


#define MY_LED_ID   1   // Id of the sensor child
#define MY_TEMP_ID  0
#define MY_TEMP_SEND_TIME 3000
int oldValue=0;
static bool state = true;
static unsigned long prevKeyEventTime=0;
static unsigned long nextTempTimeSend=0;

MyMessage msgLED(MY_LED_ID, V_STATUS);//CHILD_ID,V_LIGHT
MyMessage msgTemp(MY_TEMP_ID, V_TEMP);//CHILD_ID,V_LIGHT
void updateLedState(const bool newState){
    if (newState){
                  LED1_ON;
              }else{
                  LED1_OFF;
              }
}

void before()
{
    bool b=false;
    long l = -2147483648;
    unsigned int i = 65535/2;
    char c = 0x7f;
    soft_printf("Start demo sensor, "GRN"press SW1."RST"\r\n test Soft_Printf output bool=%? (l l=%l n=%n) (c h=%h H=%H) (i i=%i u=%u h=%h H=%H)\r\n",b,l,l,c,c,i,i,i,i);
    //_BIS_SR(LPM4_bits + GIE); // enter Low power mode 4 -
}

void setup()  
{  
    // Setup locally attached sensors
    updateLedState(state);
    unsigned int VCC = hwCPUVoltage();
    soft_printf("CPU voltage mV: %u\r\n",VCC);
}

void presentation()
{
    // Present locally attached sensors

    // Send the sketch version information to the gateway and Controller
      sendSketchInfo("LED & SW & Temp", "1.0");

      // Register all sensors to gw (they will be created as child devices)
      present(MY_LED_ID, S_BINARY,"LED");
      present(MY_TEMP_ID, S_TEMP,"MSP430 Temp");
}

void receive(const MyMessage &message){

  // We only expect one type of message from controller. But we better check anyway.
   if (message.isAck()) {
       soft_printf("This is an ack from gateway for sensor:%u\r\n",message.sensor);
       return;
   }

   unsigned char receivedMsgSensorId = message.sensor;
   unsigned char receivedMsgType = message.type;
   soft_printf("Incoming message (sensor ID %u) (msg type %u) ",receivedMsgSensorId,receivedMsgType);
   switch (receivedMsgSensorId){
           case MY_LED_ID:{
               soft_printf("LED ");
               if (receivedMsgType == V_STATUS) {
                   // Change relay state
                   state = message.getBool();
                   updateLedState(state);
                   // Write some debug info
                   soft_printf(", New status: %?",state);
               }
               break;
           }

           default:{
               soft_printf(", unknown %s",message.getString());
           }
   }

   soft_printf("\r\n");
}
void loop() 
{
    unsigned long currentLoopTime=hwMillis();
    //flagsIRQ - flaged in PORT1 interrupt when button is pressed
    if (flagsIRQ & flagKeyPressed) {
        flagsIRQ &= ~flagKeyPressed;
        unsigned long curKeyEventTime=currentLoopTime;
        if ((curKeyEventTime - prevKeyEventTime)>200){
            state = !state;
            updateLedState(state);
            prevKeyEventTime=curKeyEventTime;
            soft_printf("key pressed. key=%s\r\n"RST,state? GRN"ON":RED"OFF");
            // Value has changed from last transmission, send the updated value
//            msg().setSensor(MY_LED_ID).setType(V_STATUS).set(state);
            msgLED.set(state);
            send(msgLED);
        }
    }

    if (currentLoopTime>nextTempTimeSend){
        nextTempTimeSend=currentLoopTime+MY_TEMP_SEND_TIME;
        unsigned int temp = hwCPUTemperature();
        soft_printf("Temperature C: %u\r\n", temp);
        msgTemp.set(temp);
        send(msgTemp);
    }
    // Sleep until something happens with the sensor
//    soft_printf("wait msg -");
//    bool res = wait(5000,C_SET,V_STATUS);
//    if (res) {
//        soft_printf("received \r\n");
//        updateLedState();
//    }else{
//        soft_printf("none \r\n");
//    }


}


