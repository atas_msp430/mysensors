/* nRF24L01.h
 * Register definitions for manipulating the Nordic Semiconductor
 * nRF24L01+ RF transceiver chipsets.
 *

    Copyright (c) 2007 Stefan Engelke <mbox@stefanengelke.de>
    Some parts copyright (c) 2012 Eric Brundick <spirilis [at] linux dot com>

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without 
    restriction, including without limitation the rights to use, copy, 
    modify, merge, publish, distribute, sublicense, and/or sell copies 
    of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.
 */
#ifndef _NRF24L01_H
#define _NRF24L01_H

// PA levels
#define RF24_PA_MIN             (0)
#define RF24_PA_LOW             (1)
#define RF24_PA_HIGH            (2)
#define RF24_PA_MAX             (3)
#define MY_RF24_PA_LEVEL        (RF24_PA_HIGH)

// power level limits
#define RF24_MIN_POWER_LEVEL    (0)
#define RF24_MAX_POWER_LEVEL    (3)

// data rate
#define RF24_1MBPS              (0)
#define RF24_2MBPS              (1)
#define RF24_250KBPS            (2)
#define MY_RF24_DATARATE        (RF24_250KBPS)

// CRC
#define RF24_CRC_DISABLED       (0)
#define RF24_CRC_8              (2)
#define RF24_CRC_16             (3)

// ARD, auto retry delay
#define RF24_SET_ARD            (5) //=1500us

// ARD, auto retry count
#define RF24_SET_ARC            (15)

// nRF24L01(+) register definitions
#define RF24_REG_NRF_CONFIG     (0x00)
#define RF24_REG_EN_AA          (0x01)
#define RF24_REG_EN_RXADDR      (0x02)
#define RF24_REG_SETUP_AW       (0x03)
#define RF24_REG_SETUP_RETR     (0x04)
#define RF24_REG_RF_CH          (0x05)
#define RF24_REG_RF_SETUP       (0x06)
#define RF24_REG_STATUS         (0x07)
#define RF24_REG_OBSERVE_TX     (0x08)
#define RF24_REG_RPD            (0x09)          // nRF24L01+
#define RF24_REG_CD             (RF24_REG_RPD)  // nRF24L01
#define RF24_REG_RX_ADDR_P0     (0x0A)
#define RF24_REG_RX_ADDR_P1     (0x0B)
#define RF24_REG_RX_ADDR_P2     (0x0C)
#define RF24_REG_RX_ADDR_P3     (0x0D)
#define RF24_REG_RX_ADDR_P4     (0x0E)
#define RF24_REG_RX_ADDR_P5     (0x0F)
#define RF24_REG_TX_ADDR        (0x10)
#define RF24_REG_RX_PW_P0       (0x11)
#define RF24_REG_RX_PW_P1       (0x12)
#define RF24_REG_RX_PW_P2       (0x13)
#define RF24_REG_RX_PW_P3       (0x14)
#define RF24_REG_RX_PW_P4       (0x15)
#define RF24_REG_RX_PW_P5       (0x16)
#define RF24_REG_FIFO_STATUS    (0x17)
#define RF24_REG_DYNPD          (0x1C)
#define RF24_REG_FEATURE        (0x1D)


// dynamic payload
#define RF24_DPL_P5         (5)
#define RF24_DPL_P4         (4)
#define RF24_DPL_P3         (3)
#define RF24_DPL_P2         (2)
#define RF24_DPL_P1         (1)
#define RF24_DPL_P0         (0)

#define RF24_RF_PWR_LOW     (1)
#define RF24_RF_PWR_HIGH    (2)

// instructions
#define RF24_CMD_READ_REGISTER              (0x00)
#define RF24_CMD_WRITE_REGISTER             (0x20)
#define RF24_CMD_ACTIVATE                   (0x50)
#define RF24_CMD_READ_RX_PL_WID             (0x60)
#define RF24_CMD_READ_RX_PAYLOAD            (0x61)
#define RF24_CMD_WRITE_TX_PAYLOAD           (0xA0)
#define RF24_CMD_WRITE_ACK_PAYLOAD          (0xA8)
#define RF24_CMD_WRITE_TX_PAYLOAD_NO_ACK    (0xB0)
#define RF24_CMD_FLUSH_TX                   (0xE1)
#define RF24_CMD_FLUSH_RX                   (0xE2)
#define RF24_CMD_REUSE_TX_PL                (0xE3)
#define RF24_CMD_NOP                        (0xFF)
/* Instructions */
#define RF24_REGISTER_MASK 0x1F




/* Register Bits */
#define RF24_MASK_RX_DR  BIT6
#define RF24_MASK_TX_DS  BIT5
#define RF24_MASK_MAX_RT BIT4
#define RF24_EN_CRC      BIT3
#define RF24_CRCO        BIT2
#define RF24_PWR_UP      BIT1
#define RF24_PRIM_RX     BIT0
#define RF24_ENAA_P5     BIT5
#define RF24_ENAA_P4     BIT4
#define RF24_ENAA_P3     BIT3
#define RF24_ENAA_P2     BIT2
#define RF24_ENAA_P1     BIT1
#define RF24_ENAA_P0     BIT0
#define RF24_ERX_P5      BIT5
#define RF24_ERX_P4      BIT4
#define RF24_ERX_P3      BIT3
#define RF24_ERX_P2      BIT2
#define RF24_ERX_P1      BIT1
#define RF24_ERX_P0      BIT0
#define RF24_AW          BIT0
#define RF24_ARD         4//BIT4
#define RF24_ARC         0//BIT0
#define RF24_PLL_LOCK    BIT4
#define RF24_CONT_WAVE   BIT7
#define RF24_RF_DR       BIT3
#define RF24_RF_DR_LOW   BIT5
#define RF24_RF_DR_HIGH  BIT3
#define RF24_RF_PWR      BIT1
#define RF24_LNA_HCURR   BIT0
#define RF24_RX_DR       BIT6
#define RF24_TX_DS       BIT5
#define RF24_MAX_RT      BIT4
#define RF24_RX_P_NO     BIT1
#define RF24_TX_FULL     BIT0
#define RF24_PLOS_CNT    BIT4
#define RF24_ARC_CNT     BIT0
#define RF24_TX_REUSE    BIT6
#define RF24_FIFO_FULL   BIT5
#define RF24_TX_EMPTY    BIT4
#define RF24_RX_FULL     BIT1
#define RF24_RX_EMPTY    BIT0
#define RF24_EN_DPL      BIT2
#define RF24_EN_ACK_PAY  BIT1
#define RF24_EN_DYN_ACK  BIT0



#endif
