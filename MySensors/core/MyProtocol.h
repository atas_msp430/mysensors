
#ifndef MyProtocol_h
#define MyProtocol_h
#include <string.h>
#include <stdlib.h>
#include "MySensors/MyConfig.h"
#include "MyMessage.h"
#include "MySensorsCore.h"
#include <soft_printf.h>

extern char _fmtBuffer[];
//extern char _convBuffer[];
// parse(message, inputString)
// parse a string into a message element
// returns true if successfully parsed the input string
 bool protocolParse(MyMessage &message, char *inputString);


// Format MyMessage to the protocol representation
char *protocolFormat(MyMessage &message);

#endif
