/*
* The MySensors Arduino library handles the wireless radio link and protocol
* between your home built sensors/actuators and HA controller of choice.
* The sensors forms a self healing radio network with optional repeaters. Each
* repeater and gateway builds a routing tables in EEPROM which keeps track of the
* network topology allowing messages to be routed to nodes.
*
* Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
* Copyright (C) 2013-2018 Sensnology AB
* Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
*
* Documentation: http://www.mysensors.org
* Support Forum: http://forum.mysensors.org
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* Based on maniacbug's RF24 library, copyright (C) 2011 J. Coliz <maniacbug@ymail.com>
* RF24 driver refactored and optimized for speed and size, copyright (C) 2017 Olivier Mauti <olivier@mysensors.org>
*
* Definitions for Nordic nRF24L01+ radios:
* https://www.nordicsemi.com/eng/Products/2.4GHz-RF/nRF24L01P
*
*/

/**
* @file RF24.h
*
* @defgroup RF24grp RF24
* @ingroup internals
* @{
*
* RF24 driver-related log messages, format: [!]SYSTEM:[SUB SYSTEM:]MESSAGE
* - [!] Exclamation mark is prepended in case of error
*
* |E| SYS  | SUB  | Message              | Comment
* |-|------|------|----------------------|---------------------------------------------------------------------
* | | RF24 | INIT |                      | Initialise RF24 radio
* | | RF24 | INIT | PIN,CE=%%d,CS=%%d    | Pin configuration: chip enable (CE), chip select (CS)
* |!| RF24 | INIT | SANCHK FAIL          | Sanity check failed, check wiring or replace module
* | | RF24 | SPP  | PCT=%%d,TX LEVEL=%%d | Set TX level, input TX percent (PCT)
* | | RF24 | RBR  | REG=%%d,VAL=%%d      | Read register (REG), value=(VAL)
* | | RF24 | WBR  | REG=%%d,VAL=%%d      | Write register (REG), value=(VAL)
* | | RF24 | FRX  |                      | Flush RX buffer
* | | RF24 | FTX  |                      | Flush TX buffer
* | | RF24 | OWP  | RCPT=%%d             | Open writing pipe, recipient=(RCPT)
* | | RF24 | STL  |                      | Start listening
* | | RF24 | SPL  |                      | Stop listening
* | | RF24 | SLP  |                      | Set radio to sleep
* | | RF24 | SBY  |                      | Set radio to standby
* | | RF24 | TXM  | TO=%%d,LEN=%%d       | Transmit message to=(TO), length=(LEN)
* |!| RF24 | TXM  | MAX_RT               | Max TX retries, no ACK received
* |!| RF24 | GDP  | PYL INV              | Invalid payload size
* | | RF24 | RXM  | LEN=%%d              | Read message, length=(LEN)
* | | RF24 | STX  | LEVEL=%%d            | Set TX level, level=(LEVEL)
*
*/

#ifndef __RF24_H__
#define __RF24_H__

//#ifdef __cplusplus
//extern "C" {
//#endif

#include <MSP430.h>
#include "MySensors/hal/architecture/MyHwMSP430.h"
#include "MySensors/MyConfig.h"
#include "nrf_userconfig.h"
#include "nRF24L01.h"
#include "msp430_spi.h"


#define RF24_BROADCAST_ADDRESS	(255u)	//!< RF24_BROADCAST_ADDRESS
// powerup delay
#define RF24_POWERUP_DELAY_MS	(100u)		//!< Power up delay, allow VCC to settle, transport to become fully operational

// pipes
#define RF24_BROADCAST_PIPE		(1)		//!< RF24_BROADCAST_PIPE
#define RF24_NODE_PIPE			(0)		//!< RF24_NODE_PIPE

#define CSN_EN  RF24_CSN_POUT_NAME &= ~RF24_CSN_PIN
#define CSN_DIS RF24_CSN_POUT_NAME |= RF24_CSN_PIN
#define CE_EN   RF24_CE_POUT_NAME |= RF24_CE_PIN
#define CE_DIS  RF24_CE_POUT_NAME &= ~RF24_CE_PIN


unsigned char RF24_spiMultiByteTransfer(const unsigned char cmd, unsigned char *buf, const unsigned char len,
                                        const bool readMode);
/**
* @brief RF24_spiByteTransfer
* @param cmd
* @return
*/
unsigned char RF24_spiByteTransfer(const unsigned char cmd);
///**
//* @brief RF24_RAW_readByteRegister
//* @param cmd
//* @return
//*/
//unsigned char RF24_RAW_readByteRegister(const unsigned char cmd);
///**
//* @brief RF24_RAW_writeByteRegister
//* @param cmd
//* @param value
//* @return
//*/
//unsigned char RF24_RAW_writeByteRegister(const unsigned char cmd, const unsigned char value);


// helper macros
//#define RF24_readByteRegister(__reg) RF24_readByteRegister(__reg)//RF24_RAW_readByteRegister(RF24_CMD_READ_REGISTER | (RF24_REGISTER_MASK & (__reg)))		//!< RF24_readByteRegister
//#define RF24_writeByteRegister(__reg,__value) RF24_RAW_writeByteRegister(RF24_CMD_WRITE_REGISTER | (RF24_REGISTER_MASK & (__reg)), __value)	//!< RF24_writeByteRegister
#define RF24_writeMultiByteRegister(__reg,__buf,__len) RF24_spiMultiByteTransfer(RF24_CMD_WRITE_REGISTER | (RF24_REGISTER_MASK & (__reg)),(unsigned char *)__buf, __len,false)	//!< RF24_writeMultiByteRegister

/**
* @brief RF24_flushRX
*/
void RF24_flushRX(void);
/**
* @brief RF24_flushTX
*/
void RF24_flushTX(void);
/**
* @brief RF24_getStatus
* @return
*/
unsigned char RF24_getStatus(void);
/**
* @brief RF24_getFIFOStatus
* @return
*/
unsigned char RF24_getFIFOStatus(void);
/**
* @brief RF24_openWritingPipe
* @param recipient
*/
void RF24_openWritingPipe(const unsigned char recipient);
/**
* @brief RF24_startListening
*/
void RF24_startListening(void);
/**
* @brief RF24_stopListening
*/
void RF24_stopListening(void);
/**
* @brief RF24_sleep
*/
void RF24_sleep(void);
/**
* @brief RF24_standBy
*/
void RF24_standBy(void);
/**
* @brief RF24_powerDown
*/
void RF24_powerDown(void);
/**
* @brief RF24_powerUp
*/
void RF24_powerUp(void);
/**
* @brief RF24_sendMessage
* @param recipient
* @param buf
* @param len
* @param noACK set True if no ACK is required
* @return
*/
bool RF24_sendMessage(const unsigned char recipient, const void *buf, const unsigned char len,
                            const bool noACK = false);
/**
* @brief RF24_getDynamicPayloadSize
* @return
*/
unsigned char RF24_getDynamicPayloadSize(void);
/**
* @brief RF24_isDataAvailable
* @return
*/
bool RF24_isDataAvailable(void);
/**
* @brief RF24_readMessage
* @return
*/
unsigned char RF24_readMessage(void *buf);
/**
* @brief RF24_setNodeAddress
* @param address
*/
void RF24_setNodeAddress(const unsigned char address);
/**
* @brief RF24_getNodeID
* @return
*/
unsigned char RF24_getNodeID(void);
/**
* @brief RF24_sanityCheck
* @return
*/
bool RF24_sanityCheck(void);
/**
* @brief RF24_initialize
* @return
*/

bool RF24_initialize(void);
void RF24_IRQ_Setup();
void RF24_HW_init();
/**
* @brief RF24_setChannel
* @param channel
*/
void RF24_setChannel(const unsigned char channel);
/**
* @brief RF24_setRetries
* @param retransmitDelay
* @param retransmitCount
*/
void RF24_setRetries(const unsigned char retransmitDelay, const unsigned char retransmitCount);
/**
* @brief RF24_setAddressWidth
* @param addressWidth
*/
void RF24_setAddressWidth(const unsigned char addressWidth);
/**
* @brief RF24_setRFSetup
* @param RFsetup
*/
void RF24_setRFSetup(const unsigned char RFsetup);
/**
* @brief RF24_setFeature
* @param feature
*/
void RF24_setFeature(const unsigned char feature);
/**
* @brief RF24_getFeature
* @return
*/
unsigned char RF24_getFeature(void);
/**
* @brief RF24_setPipe
* @param pipe
*/
void RF24_setPipe(const unsigned char pipe);
/**
* @brief RF24_setAutoACK
* @param pipe
*/
void RF24_setAutoACK(const unsigned char pipe);
/**
* @brief RF24_setDynamicPayload
* @param pipe
*/
void RF24_setDynamicPayload(const unsigned char pipe);
/**
* @brief RF24_setRFConfiguration
* @param configuration
*/
void RF24_setRFConfiguration(const unsigned char configuration);
/**
* @brief RF24_setPipeAddress
* @param pipe
* @param address
* @param addressWidth
*/
void RF24_setPipeAddress(const unsigned char pipe, unsigned char *address, const unsigned char addressWidth);
/**
* @brief RF24_setPipeLSB
* @param pipe
* @param LSB
*/
void RF24_setPipeLSB(const unsigned char pipe, const unsigned char LSB);
/**
* @brief RF24_getObserveTX
* @return
*/
unsigned char RF24_getObserveTX(void);
/**
* @brief RF24_setStatus
* @param status
*/
void RF24_setStatus(const unsigned char status);
/**
* @brief RF24_enableFeatures
*/
void RF24_enableFeatures(void);
/**
* @brief RF24_getTxPowerPercent
* @return
*/
unsigned char RF24_getTxPowerPercent(void);
/**
* @brief RF24_getTxPowerLevel
* @return
*/
int RF24_getTxPowerLevel(void);
/**
* @brief RF24_setTxPowerPercent
* @param newPowerPercent
* @return
*/
bool RF24_setTxPowerPercent(const unsigned char newPowerPercent);
/**
* @brief RF24_getSendingRSSI
* @return Pseudo-RSSI based on ARC register
*/
int RF24_getSendingRSSI(void);
/**
* @brief Generate a constant carrier wave at active channel & transmit power (for testing only).
*/
void RF24_enableConstantCarrierWave(void) __attribute__((unused));
/**
* @brief Stop generating a constant carrier wave (for testing only).
*/
void RF24_disableConstantCarrierWave(void) __attribute__((unused));
/**
* @brief Retrieve latched RPD power level, in receive mode (for testing, nRF24L01+ only).
* @return True when power level >-64dBm for more than 40us.
*/
bool RF24_getReceivedPowerDetector(void) __attribute__((unused));

#if defined(MY_RX_MESSAGE_BUFFER_FEATURE)
/**
* @brief Callback type
*/
typedef void (*RF24_receiveCallbackType)(void);
/**
* @brief RF24_registerReceiveCallback
* Register a callback, which will be called (from interrupt context) for every message received.
* @note When a callback is registered, it _must_ retrieve the message from the nRF24
* by calling RF24_readMessage(). Otherwise the interrupt will not get deasserted
* and message reception will stop.
* @param cb
*/
void RF24_registerReceiveCallback(RF24_receiveCallbackType cb);
#endif


#endif // __RF24_H__

/** @}*/
