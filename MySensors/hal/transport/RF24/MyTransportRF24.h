/*
 * MyTransportRF24.h
 *
 *  Created on: 7 ����. 2019 �.
 *      Author: Atas
 */
#pragma once
//#ifndef MYTRANSPORTRF24_H_
//#define MYTRANSPORTRF24_H_
#include "MySensors/hal/transport/RF24/RF24.h"
#include "MySensors/core/MyMessage.h"
#include "MyHwMSP430.h"

#ifdef __cplusplus
extern "C" {
#endif

#define INVALID_SNR         ((int)-256) //!< INVALID_SNR
#define INVALID_RSSI        ((int)-256) //!< INVALID_RSSI
#define INVALID_PERCENT     ((int)-100) //!< INVALID_PERCENT
#define INVALID_LEVEL       ((int)-256) //!< INVALID_LEVEL

#if defined(MY_RX_MESSAGE_BUFFER_FEATURE)
    #if defined(MY_RADIO_NRF5_ESB)
        #error Receive message buffering not supported for NRF5 radio! Please define MY_NRF5_RX_BUFFER_SIZE
    #endif
    #if defined(MY_RADIO_RFM69)
        #error Receive message buffering not supported for RFM69!
    #endif
    #if defined(MY_RADIO_RFM95)
        #error Receive message buffering not supported for RFM95!
    #endif
    #if defined(MY_RS485)
        #error Receive message buffering not supported for RS485!
    #endif
#elif defined(MY_RX_MESSAGE_BUFFER_SIZE)
#error Receive message buffering requires message buffering feature enabled!
#endif

/**
* @brief Signal report selector
*/
typedef enum {
    SR_RX_RSSI,            //!< SR_RX_RSSI
    SR_TX_RSSI,            //!< SR_TX_RSSI
    SR_RX_SNR,             //!< SR_RX_SNR
    SR_TX_SNR,             //!< SR_TX_SNR
    SR_TX_POWER_LEVEL,     //!< SR_TX_POWER_LEVEL
    SR_TX_POWER_PERCENT,   //!< SR_TX_POWER_PERCENT
    SR_UPLINK_QUALITY,     //!< SR_UPLINK_QUALITY
    SR_NOT_DEFINED         //!< SR_NOT_DEFINED
} signalReport_te;


/**
* @brief Initialize transport HW
* @return true if initialization successful
*/
 bool transportInit(void);
/**
* @brief Set node address
*/
 void transportSetAddress(const unsigned char address);
/**
* @brief Retrieve node address
*/
 unsigned char transportGetAddress(void) ;//__attribute__((unused));
/**
* @brief Send message
* @param to recipient
* @param data message to be sent
* @param len length of message (header + payload)
* @param noACK do not wait for ACK
* @return true if message sent successfully
*/
 bool transportSend(const unsigned char to, const void *data, const unsigned char len,
                   const bool noACK = false);
/**
* @brief Verify if RX FIFO has pending messages
* @return true if message available in RX FIFO
*/
 bool transportAvailable(void);
/**
* @brief Sanity check for transport: is transport HW still responsive?
* @return true if transport HW is ok
*/
 bool transportSanityCheck(void);
/**
* @brief Receive message from FIFO
* @return length of received message (header + payload)
*/
 unsigned char transportReceive(void *data);
/**
* @brief Power down transport HW (if corresponding MY_XYZ_POWER_PIN defined)
*/
 void transportPowerDown(void);
/**
* @brief Power up transport HW (if corresponding MY_XYZ_POWER_PIN defined)
*/
 void transportPowerUp(void);
/**
* @brief Set transport HW to sleep (no power down)
*/
 void transportSleep(void);
/**
* @brief Set transport HW to standby
*/
 void transportStandBy(void);
/**
* @brief transportGetSendingRSSI
* @return RSSI of outgoing message (via ACK packet)
*/
 int transportGetSendingRSSI(void);
/**
* @brief transportGetReceivingRSSI
* @return RSSI of incoming message
*/
 int transportGetReceivingRSSI(void);
/**
* @brief transportGetSendingSNR
* @return SNR of outgoing message (via ACK packet)
*/
 int transportGetSendingSNR(void);
/**
* @brief transportGetReceivingSNR
* @return SNR of incoming message
*/
 int transportGetReceivingSNR(void);
/**
* @brief transportGetTxPowerPercent
* @return TX power level in percent
*/
 int transportGetTxPowerPercent(void);
/**
* @brief transportSetTxPowerPercent
* @param powerPercent power level in percent
* @return True if power level set
*/
 bool transportSetTxPowerPercent(const unsigned char powerPercent) ;//__attribute__((unused));
/**
* @brief transportGetTxPowerLevel
* @return TX power in dBm
*/
 int transportGetTxPowerLevel(void);

#ifdef __cplusplus
}
#endif
//#endif /* MYSENSORS_HAL_TRANSPORT_RF24_MYTRANSPORTRF24_H_ */
