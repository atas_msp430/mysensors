/* msp430_spi.c
 * Library for performing SPI I/O on a wide range of MSP430 chips.
 *
 * Serial interfaces supported:
 * 1. USI - developed on MSP430G2231
 * 2. USCI_A - developed on MSP430G2553
 * 3. USCI_B - developed on MSP430G2553
 * 4. USCI_A F5xxx - developed on MSP430F5172, added F5529
 * 5. USCI_B F5xxx - developed on MSP430F5172, added F5529
 *
 * Copyright (c) 2013, Eric Brundick <spirilis@linux.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright notice
 * and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT,
 * OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 * ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include "msp430_spi.h"

#ifdef __cplusplus
extern "C" {
#endif

//#include "nrf_userconfig.h"

void spi_init()
{
	/* Configure ports on MSP430 device for USCI_B */
	P1SEL |= BIT5 | BIT6 | BIT7;
	P1SEL2 |= BIT5 | BIT6 | BIT7;

	/* USCI-B specific SPI setup */
	UCB0CTL1 |= UCSWRST;
	UCB0CTL0 = UCCKPH | UCMSB | UCMST | UCMODE_0 | UCSYNC;  // SPI mode 0, master
	UCB0BR0 = 0x04;  // SPI clocked at same speed as SMCLK
	UCB0BR1 = 0x00;
	UCB0CTL1 |= UCSSEL_2;  // Clock = SMCLK, clear UCSWRST and enables USCI_B module.
	UCB0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
	IE2 |= UCB0RXIE;
	_EINT();
}

// function in header
//inline unsigned char spi_transfer(unsigned char inb) { UCB0TXBUF = inb; LPM0; return UCB0RXBUF; }
unsigned char spi_transfer(const unsigned char inb)
{
//    LED1_ON;
    UCB0TXBUF = inb;
    LPM0;
//    LED1_OFF;
    return UCB0RXBUF;
}

unsigned int spi_transfer16(unsigned int inw)
{
	unsigned int retw;
	unsigned char *retw8 = (unsigned char *)&retw, *inw8 = (unsigned char *)&inw;

	UCB0TXBUF = inw8[1];
//	while ( !(IFG2 & UCB0RXIFG) );
//	LED1_ON;
	LPM0;
//	LED1_OFF;
	retw8[1] = UCB0RXBUF;
	UCB0TXBUF = inw8[0];
//	while ( !(IFG2 & UCB0RXIFG) )		;
//	LED1_ON;
	LPM0;
	retw8[0] = UCB0RXBUF;
//	LED1_OFF;
	return retw;
}


//#pragma vector=USCIAB0RX_VECTOR
//__interrupt void USCIA0RX_ISR(void)
//{
//    if(IFG2 & UCA0RXIFG)
//       {
//         // do UART stuff
//       }
//       if(IFG2 & UCB0RXIFG)
//       {
//         // do SPI stuff
//           IFG2 &= ~UCB0RXIFG;
//           LPM4_EXIT;
//       }
//
//}



#ifdef __cplusplus
}
#endif

