/*
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2018 Sensnology AB
 * Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 */


#include "MyMessage.h"
//#include <stdio.h>

//#include "../MySensors.h"

MyMessage::MyMessage(void)
{
	clear();
}

MyMessage::MyMessage(const unsigned char _sensor, const unsigned char _type)
{
	clear();
	sensor = _sensor;
	type   = _type;
}

void MyMessage::clear(void)
{
	last                = 0u;
	sender              = 0u;
	destination         = 0u;       // Gateway is default destination
	version_length      = 0u;
	command_ack_payload = 0u;
	type                = 0u;
	sensor              = 0u;
	(void)memset(data, 0u, sizeof(data));

	// set message protocol version
	miSetVersion(PROTOCOL_VERSION);
}

bool MyMessage::isAck(void) const
{
	return miGetAck();
}

unsigned char MyMessage::getCommand(void) const
{
	return miGetCommand();
}

/* Getters for payload converted to desired form */
void* MyMessage::getCustom(void) const
{
	return (void *)data;
}

char MyMessage::_i2h(const unsigned char i) const
{
	unsigned char k = i & 0x0F;
	if (k <= 9) {
		return '0' + k;
	} else {
		return 'A' + k - 10;
	}
}

char* MyMessage::getCustomString(char *buffer) const
{
    char * strBuf = buffer;
     char const* dataBuf = data;

    unsigned char dataRemain = miGetLength();
    while (dataRemain--){
        char c = *dataBuf++;
        *strBuf++= hex(c >> 4);
        *strBuf++= hex(c);
    }
    *strBuf++= '\0';
//	for (unsigned char i = 0; i < miGetLength(); i++) {
//		buffer[i * 2] = i2h(data[i] >> 4);
//		buffer[(i * 2) + 1] = i2h(data[i]);
//	}
//	buffer[miGetLength() * 2] = '\0';
	return buffer;
}

char* MyMessage::getStream(char *buffer) const
{
    if (buffer == nullptr) {
        return nullptr;
    }
	unsigned char cmd = miGetCommand();
    if (cmd != C_STREAM) {
        return nullptr;
    }

return getCustomString(buffer);
}

const char* MyMessage::getString() const
{
    unsigned char payloadType = miGetPayloadType();
    if (payloadType == P_STRING) {
        return data;
    } else {
        return nullptr;
    }
}

const char* MyMessage::getString(char *buffer) const
{
    if (buffer == nullptr)
        return data;
    unsigned char payloadType = miGetPayloadType();
    //	if (buffer != nullptr)
    switch (payloadType){
            case P_STRING:{
                return data;
            }
            case P_BYTE:{
                snprintf_P(buffer,"%u",bValue);
                break;
            }
            case P_INT16:{
                snprintf_P(buffer,"%i",iValue);
                break;
            }
            case P_UINT16:{
                snprintf_P(buffer,"%u",uiValue);
                break;
            }
            case P_LONG32:{
                snprintf_P(buffer,"%l",lValue);
                break;
            }
            case P_ULONG32:{
                snprintf_P(buffer,"%n",ulValue);
                break;
            }
            case P_CUSTOM:{
                getCustomString(buffer);
                break;
            }
            default:{
                break;
            }
    }
return buffer;
}

//		} else if (payloadType == P_INT16) {
//			(void)itoa(iValue, buffer);
//		} else if (payloadType == P_UINT16) {
//			(void)itoa(uiValue, buffer);
//		} else if (payloadType == P_LONG32) {
//			(void)itoa(lValue, buffer);
//		} else if (payloadType == P_ULONG32) {
//			(void)itoa(ulValue, buffer);
//		} else if (payloadType == P_FLOAT32) {
////			(void)dtostrf(fValue, 2, min(fPrecision, (unsigned char)8), buffer);
//		} else if (payloadType == P_CUSTOM) {
//			return getCustomString(buffer);
//		}
//		return buffer;
//	} else {
//		return nullptr;
//	}
//}

bool MyMessage::getBool(void) const
{
	return getByte();
}

unsigned char MyMessage::getByte(void) const
{
	if (miGetPayloadType() == P_BYTE) {
		return data[0];
	} else if (miGetPayloadType() == P_STRING) {
		return atoi(data);
	} else {
		return 0;
	}
}


//float MyMessage::getFloat(void) const
//{
//	if (miGetPayloadType() == P_FLOAT32) {
//		return fValue;
//	} else if (miGetPayloadType() == P_STRING) {
//		return atof(data);
//	} else {
//		return 0;
//	}
//}

long MyMessage::getLong(void) const
{
	if (miGetPayloadType() == P_LONG32) {
		return lValue;
	} else if (miGetPayloadType() == P_STRING) {
		return atol(data);
	} else {
		return 0;
	}
}

unsigned long MyMessage::getULong(void) const
{
	if (miGetPayloadType() == P_ULONG32) {
		return ulValue;
	} else if (miGetPayloadType() == P_STRING) {
		return atol(data);
	} else {
		return 0;
	}
}

int MyMessage::getInt(void) const
{
	if (miGetPayloadType() == P_INT16) {
		return iValue;
	} else if (miGetPayloadType() == P_STRING) {
		return atoi(data);
	} else {
		return 0;
	}
}

unsigned int MyMessage::getUInt(void) const
{
	if (miGetPayloadType() == P_UINT16) {
		return uiValue;
	} else if (miGetPayloadType() == P_STRING) {
		return atoi(data);
	} else {
		return 0;
	}

}

MyMessage& MyMessage::setType(const unsigned char _type)
{
    type = _type;
	return *this;
}

MyMessage& MyMessage::setSensor(const unsigned char _sensor)
{
	sensor = _sensor;
	return *this;
}

MyMessage& MyMessage::setDestination(const unsigned char _destination)
{
	destination = _destination;
	return *this;
}

// Set payload
MyMessage& MyMessage::set(const void* value, const unsigned char length)
{
	unsigned char payloadLength = value == nullptr ? 0 : min(length, (unsigned char)MAX_PAYLOAD);
	miSetLength(payloadLength);
	miSetPayloadType(P_CUSTOM);
	memcpy(data, value, payloadLength);
	return *this;
}

MyMessage& MyMessage::set(const char* value)
{
	unsigned char length = (value == nullptr) ? 0 : min(strlen(value), (size_t)MAX_PAYLOAD);
	miSetLength(length);
	miSetPayloadType(P_STRING);
	if (length) {
		strncpy(data, value, length);
	}
	// null terminate string
	data[length] = 0;
	return *this;
}

#if !defined(__linux__)
//MyMessage& MyMessage::set(const __FlashStringHelper* value)
//{
//	unsigned char length = value == nullptr ? 0
//	                 : min(strlen_P(reinterpret_cast<const char *>(value)), (size_t)MAX_PAYLOAD);
//	miSetLength(length);
//	miSetPayloadType(P_STRING);
//	if (length) {
//		strncpy_P(data, reinterpret_cast<const char *>(value), length);
//	}
//	// null terminate string
//	data[length] = 0;
//	return *this;
//}
#endif


MyMessage& MyMessage::set(const bool value)
{
	miSetLength(1);
	miSetPayloadType(P_BYTE);
	data[0] = value;
	return *this;
}

MyMessage& MyMessage::set(const unsigned char value)
{
	miSetLength(1);
	miSetPayloadType(P_BYTE);
	data[0] = value;
	return *this;
}

//MyMessage& MyMessage::set(const float value, const unsigned char decimals)
//{
//	miSetLength(5); // 32 bit float + persi
//	miSetPayloadType(P_FLOAT32);
//	fValue=value;
//	fPrecision = decimals;
//	return *this;
//}

MyMessage& MyMessage::set(const unsigned long value)
{
	miSetPayloadType(P_ULONG32);
	miSetLength(4);
	ulValue = value;
	return *this;
}

MyMessage& MyMessage::set(const long value)
{
	miSetPayloadType(P_LONG32);
	miSetLength(4);
	lValue = value;
	return *this;
}

MyMessage& MyMessage::set(const unsigned int value)
{
	miSetPayloadType(P_UINT16);
	miSetLength(2);
	uiValue = value;
	return *this;
}

MyMessage& MyMessage::set(const int value)
{
	miSetPayloadType(P_INT16);
	miSetLength(2);
	iValue = value;
	return *this;
}
