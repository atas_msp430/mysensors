/*
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2018 Sensnology AB
 * Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 */

#include "MyHwMSP430.h"
#include "stdint.h"

#include "MySensors/core/MyMessage.h"
//#ifdef DEBUG_OUTPUT_ENABLED
#include "soft_serial.h"
#include "soft_printf.h"
//#endif

#define MY_WAKE_UP_BY_TIMER         ((signed char)-1)       //!< Sleeping wake up by timer
#define MY_SLEEP_NOT_POSSIBLE       ((signed char)-2)       //!< Sleeping not possible
#define INTERRUPT_NOT_DEFINED       ((unsigned char)255)    //!< _sleep() param: no interrupt defined
#define MODE_NOT_DEFINED                ((unsigned char)255)    //!< _sleep() param: no mode defined
#define VALUE_NOT_DEFINED               ((unsigned char)255)    //!< Value not defined
#define FUNCTION_NOT_SUPPORTED  ((unsigned int)0)       //!< Function not supported

static volatile unsigned long timerCounterHwMillis;
static volatile unsigned long timerCounterDelayMs;

volatile unsigned char flagsIRQ = 0;
#ifdef MY_GATEWAY_SERIAL
char _serialInputString[MY_GATEWAY_MAX_RECEIVE_LENGTH];    // A buffer for incoming commands from serial interface
volatile unsigned char _serialInputPos;

extern char _fmtBuffer[MY_GATEWAY_MAX_SEND_LENGTH];
//extern char _convBuffer[MAX_PAYLOAD*2+1];
#endif

bool hwInit(void)
{
    WDTCTL = WDTHOLD | WDTPW;       // Stop Watch dog timer
    P3SEL = 0; P3DIR = 0xFF; P3OUT = 0; P3REN = 0;
    P2SEL = 0; P2DIR = 0xFF; P2OUT = 0; P2REN = 0; P2IE=0; P2IES=0; P2IFG=0;
    P1SEL = 0; P1DIR = 0xFF; P1OUT = 0; P1REN = 0; P1IE=0; P1IES=0; P1IFG=0;

    P1DIR |= LED1+LED2;                    //LED

    P1IES &= ~BUTTON;           //Interrupt Edge Select 0= low-to-high 1=high-to-low
    P1DIR &= ~BUTTON;
    P1REN |= BUTTON;            // resistor enabled
//    P1OUT &= ~BUTTON;         // pull-down
    P1OUT |= BUTTON;            // pull-up after that Interrupt Flag raised, so need to be cleared, otherwise unwanted interrupt occur.
    P1IE |= BUTTON;             // P1.3 interrupt enabled
    _delay_cycles(100);
    P1IFG &= ~BUTTON;           // P1.3 IFG cleared

#if (!defined BSP_CONFIG_CLOCK_MHZ_SELECT)
 #error "ERROR: need define BSP_CONFIG_CLOCK_MHZ_SELECT in SimpliciTi\components\bsp\boards\LP_G2553\bsp_config.h "
#elif (BSP_CONFIG_CLOCK_MHZ_SELECT == 1)
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
//    UCOS16=0;
#elif (BSP_CONFIG_CLOCK_MHZ_SELECT == 8)
    DCOCTL = CALDCO_8MHZ;
    BCSCTL1 = CALBC1_8MHZ;

#elif (BSP_CONFIG_CLOCK_MHZ_SELECT == 16)
    DCOCTL = CALDCO_16MHZ;
    BCSCTL1 = CALBC1_16MHZ;

#else
    #error "ERROR: unknown CPU speed"
#endif
    _delay_cycles(10000);
//#ifdef DEBUG_OUTPUT_ENABLED
    serial_setup(softTXD, softRXD, ((16000000/1024)*1000) / 115200);
//#endif


    //-------------------- Timer A setup ----------------
     timerCounterHwMillis = 0;
     timerCounterDelayMs = 0;
     CCTL0 = CCIE;                             // CCR0 interrupt enabled
     CCR0 = 12; //~1ms
     BCSCTL3 |= LFXT1S_2; //VLO
     TACTL = TASSEL_1 + MC_1 + ID_0;  //12000kHz
     //------------------------------
     __enable_interrupt();
#if defined(MY_GATEWAY_SERIAL)
//#if !defined(MY_DISABLED_SERIAL)
//	MY_SERIALDEVICE.begin(MY_BAUD_RATE);
    UART_init();
    snprintf_P(_fmtBuffer,
//               MY_GATEWAY_MAX_SEND_LENGTH,
               "hwInit:P1OUT=%b P1DIR=%b\r\n",P1OUT, P1DIR);
    uart_puts(_fmtBuffer);

    //	while (!MY_SERIALDEVICE) {}
  //  #endif
#endif
//	if (EEPROM.init() == EEPROM_OK) {}
//		uint16 cnt;
//		EEPROM.count(&cnt);
//		if(cnt>=EEPROM.maxcount()) {
//			// tmp, WIP: format eeprom if full
//			EEPROM.format();
//		}
//		return true;
//	}
//	return false;
return true;
}

void hwReadConfigBlock(void *buf, void *addr, size_t length)
{
//	unsigned char *dst = static_cast<unsigned char *>(buf);
//	int pos = reinterpret_cast<int>(addr);
//	while (length-- > 0) {
//		*dst++ = EEPROM.read(pos++);
//	}
}

void hwWriteConfigBlock(void *buf, void *addr, size_t length)
{
//	unsigned char *src = static_cast<unsigned char *>(buf);
//	int pos = reinterpret_cast<int>(addr);
//	while (length-- > 0) {
//		EEPROM.write(pos++, *src++);
//	}
}

unsigned char hwReadConfig(const int addr)
{
	unsigned char value;
	hwReadConfigBlock(&value, reinterpret_cast<void *>(addr), 1);
	return value;
}

void hwWriteConfig(const int addr, unsigned char value)
{
	hwWriteConfigBlock(&value, reinterpret_cast<void *>(addr), 1);
}

signed char hwSleep(unsigned char interrupt1, unsigned char mode1, unsigned char interrupt2, unsigned char mode2, unsigned long ms)
{
    return hwSleep(ms);
}
signed char hwSleep(unsigned char interrupt1, unsigned char mode1, unsigned long ms)
{
    return hwSleep(ms);
}

signed char hwSleep(unsigned long ms)
{
    timerCounterDelayMs = (ms>>2) | 1; // set variable counter for WD
    WDTCTL = WDT_ADLY_1_9;              // ~ 1.9 ms
    IE1 |= WDTIE; // enable WD ISR
    LPM0; // WD disable inside WD isr
    WDTCTL = WDTHOLD | WDTPW;
    IE1 &= ~WDTIE;

    if (timerCounterDelayMs){
        return true;
    }
    return MY_WAKE_UP_BY_TIMER;
}

void hwRandomNumberInit(void)
{
//	// use internal temperature sensor as noise source
//	adc_reg_map *regs = ADC1->regs;
//	regs->CR2 |= ADC_CR2_TSVREFE;
//	regs->SMPR1 |= ADC_SMPR1_SMP16;
//
//	unsigned long seed = 0;
//	unsigned int currentValue = 0;
//	unsigned int newValue = 0;
//
//	for (unsigned char i = 0; i < 32; i++) {
//		const unsigned long timeout = hwMillis() + 20;
//		while (timeout >= hwMillis()) {
//			newValue = adc_read(ADC1, 16);
//			if (newValue != currentValue) {
//				currentValue = newValue;
//				break;
//			}
//		}
//		seed ^= ( (newValue + hwMillis()) & 7) << i;
//	}
//	randomSeed(seed);
//	regs->CR2 &= ~ADC_CR2_TSVREFE; // disable VREFINT and temp sensor
}

bool hwUniqueID(unique_id_t *uniqueID)
{
//	(void)memcpy((unsigned char *)uniqueID, (unsigned long *)0x1FFFF7E0, 16); // FlashID + ChipID
	return true;
}

unsigned int hwCPUVoltage()
{
    unsigned int        vccRawValue;
    unsigned int        vccValue_mV;
    ADC10CTL0 =   SREF_1        /* VR+ = VREF+ = 2.5V and VR- = AVSS */
                | REF2_5V       /* ADC10 Ref 2.5V */
                | REFON         /* ADC10 Reference on */
                | ADC10SHT_3    /* 64 x ADC10CLKs */
                | ADC10ON       /* ADC10 On/Enable */
                | ADC10IE;      /* ADC10 Interrupt Enalbe */

    ADC10CTL1 =   INCH_11       /* Internal VCC/2 Channel */
//                | SHS_0         /* ADC10SC */
                | ADC10DIV_3    /* Divider 0 */
 //               | ADC10SSEL_0  /* ADC10SC clock source*/
                ;
    //WD_LP0_100us;
hwSleep(30);
    // start conversion and wait for it

    ADC10CTL0 |= ENC | ADC10SC;
    LPM0;
    // stop conversion and turn off ADC in ISR
    vccRawValue = ADC10MEM;

    //soft_printf("VCC raw=%u ",vccRawValue);
    vccValue_mV = ((unsigned long)vccRawValue * 5000) >>10;
return vccValue_mV;
}

unsigned int hwCPUFrequency(void)
{
//	return F_CPU/100000UL;
    return BSP_CONFIG_CLOCK_MHZ_SELECT;
}
//T = (((ADC_value * 1500mV) / 1023) - 986) / 3.55
signed char hwCPUTemperature(void) //T = ((ADC_value - 672) * 423) / 1023
{
    unsigned int rawTempValue;
    int tempValue;

    ADC10CTL0 =       SREF_1        /* VR+ = VREF+ = use default 1.5V and VR- = AVSS */
                    | REFON         /* ADC10 Reference on */
                    | ADC10SHT_3    /* 64 x ADC10CLKs */
                    | ADC10ON       /* ADC10 On/Enable */
                    | ADC10IE;      /* ADC10 Interrupt Enable */

    ADC10CTL1 =   INCH_10       /* Internal Temperature Sensor */
//                    | SHS_0         /* ADC10SC */
                    | ADC10DIV_3    /* Divider 0 */
 //                   | ADC10SSEL_0  /* ADC10SC clock source*/
                    ;
        //WD_LP0_100us;
    hwSleep(30);
   // start conversion and wait for it
    ADC10CTL0 |= ENC | ADC10SC;
    LPM0;
    // stop conversion and turn off ADC
    rawTempValue = ADC10MEM;

    //soft_printf("temp raw=%u ",rawTempValue);

    tempValue = ((rawTempValue - 672)*423)>>10;
    return tempValue;
}

unsigned int hwFreeMem(void)
{
	//Not yet implemented
	return FUNCTION_NOT_SUPPORTED;
}

void hwDebugPrint(const char *fmt, ...)
{
#ifndef MY_DISABLED_SERIAL
//	char fmtBuffer[MY_SERIAL_OUTPUT_SIZE];
#ifdef MY_GATEWAY_SERIAL
	// prepend debug message to be handled correctly by controller (C_INTERNAL, I_LOG_MESSAGE)
//	snprintf_P(fmtBuffer, sizeof(fmtBuffer), PSTR("0;255;%u;0;%u;%u "), C_INTERNAL, I_LOG_MESSAGE, hwMillis());
//	MY_DEBUGDEVICE.print(fmtBuffer);
#else
	// prepend timestamp
//	MY_DEBUGDEVICE.print(hwMillis());
//	MY_DEBUGDEVICE.print(F(" "));
#endif
//	va_list args;
//	va_start(args, fmt);
//	vsnprintf_P(fmtBuffer, sizeof(fmtBuffer), fmt, args);
#ifdef MY_GATEWAY_SERIAL
	// Truncate message if this is gateway node
//	fmtBuffer[sizeof(fmtBuffer) - 2] = '\n';
//	fmtBuffer[sizeof(fmtBuffer) - 1] = '\0';
#endif
//	va_end(args);
//	MY_DEBUGDEVICE.print(fmtBuffer);
	// Disable flush since current STM32duino implementation performs a reset
	// instead of an actual flush
	//MY_DEBUGDEVICE.flush();
#else
	(void)fmt;
#endif
}

void hwWatchdogReset()
{
//    LOG_GREEN("hwWatchdog ms=%l\n\r",hwMillis());
//    LPM4;
//    WDTCTL = WDTPW+WDTCNTCL; //clear WD timer
}

unsigned long hwMillis(void)
{
    return timerCounterHwMillis;
}

void hwReboot()
{
//    WDTCTL=0;
}


//=============================
// Timer 0  Interrupt
//=============================
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
  timerCounterHwMillis++;
}

//=============================
// PORT1 Interrupt
//=============================
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{

    if (P1IFG & BUTTON)
    {
        if (P1IES & BUTTON) {
//            P1OUT |= LED1;
        }
        else{
//            P1OUT &= ~LED1;
            flagsIRQ |= flagKeyPressed;
            LPM4_EXIT;
        }

        P1IFG &= ~BUTTON; // P1.3 IFG cleared
        //P1IES ^= BUTTON;     // toggle the interrupt edge,
    }

}


//=============================
// WatchDog Interrupt
//=============================

#pragma vector=WDT_VECTOR
__interrupt void watchdog_timer (void)
{
    if (timerCounterDelayMs){
        timerCounterDelayMs--;
    }else
    {
        WDTCTL = WDTHOLD | WDTPW; //this code just in case WD launched by #defined LPM mode;
        IE1 &= ~WDTIE;
        LPM4_EXIT;
    }

}

//=============================
// ADC10 Interrupt - triggered after Volt measured
//=============================

#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void)
{
    ADC10CTL0 &= ~ENC;
    ADC10CTL0 &= ~(ADC10IFG | ADC10ON | REFON | ADC10IE);
  //  ADC10CTL0 = 0;
//    ADC10CTL1 = 0;
    //ADC10AE0 = 0;
    LPM3_EXIT;
}
//=============================
// UART & SPI Interrupt
//=============================

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCIA0RX_ISR(void)
{
    if(IFG2 & UCB0RXIFG)
        {
            // do SPI stuff
            IFG2 &= ~UCB0RXIFG;
            LPM4_EXIT;
        }
#ifdef MY_GATEWAY_SERIAL
    if(IFG2 & UCA0RXIFG)
    {
        // do UART stuff
        unsigned char rxByte=UCA0RXBUF;
//        if (!_serialMessageNeedProcess){
        _serialInputString[_serialInputPos] = rxByte;
        if (rxByte == '\n') {
            _serialInputString[_serialInputPos] = 0 ;
            _serialInputPos = 0;
            _serialMessageNeedProcess = true;

        }
        else
            if (_serialInputPos < MY_GATEWAY_MAX_RECEIVE_LENGTH)
                {
                    _serialInputString[_serialInputPos++] = rxByte;
                }
            else{
                    _serialInputPos = 0;
                }

    }
#endif

}
