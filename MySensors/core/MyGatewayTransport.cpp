/*
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Tomas Hozza <thozza@gmail.com>
 * Copyright (C) 2015  Tomas Hozza
 * Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 */

#include "MySensors/MyConfig.h"
#ifdef MY_GATEWAY_SERIAL
#include "MyGatewayTransport.h"
#include "MyIndication.h"
#include "soft_printf.h"
#include "Version.h"

//#include "MySensorsCore.h"
//#include "MyGatewayTransportSerial.cpp"

//extern bool transportSendRoute(MyMessage &message);

//// global variables
//extern MyMessage _msg;
//extern MyMessage _msgTmp;


// global variables


//char _serialInputString[MY_GATEWAY_MAX_RECEIVE_LENGTH];    // A buffer for incoming commands from serial interface
//volatile unsigned char _serialInputPos;
volatile bool _serialMessageNeedProcess = false;

MyMessage _serialMsg;


bool gatewayTransportSend(MyMessage &message)
{
//    LOG;
    setIndication(INDICATION_GW_TX);
    char * buf = protocolFormat(message);
    uart_puts(buf);
    DEBUG_OUTPUT("gatewayTransportSend:%s\n\r",buf);
    // Serial print is always successful
    return true;
}

bool gatewayTransportInit(void)
{
//    LOG;
    (void)gatewayTransportSend(buildGw(_msgTmp, I_GATEWAY_READY).set(MSG_GW_STARTUP_COMPLETE));
    // Send presentation of locally attached sensors (and node if applicable)
    presentNode();
    return true;
}

bool gatewayTransportAvailable(void)
{
    if (_serialMessageNeedProcess){
        const bool ok = protocolParse(_serialMsg, _serialInputString);
        if (ok) {
            setIndication(INDICATION_GW_RX);
        }
        _serialMessageNeedProcess=false;
        return ok;
    }
return false;
}


MyMessage & gatewayTransportReceive(void)
{

    // Return the last parsed message
    return _serialMsg;

}


void gatewayTransportProcess(void)
{
//    LOG;
	if (gatewayTransportAvailable()) {
		_msg = gatewayTransportReceive();
		if (_msg.destination == GATEWAY_ADDRESS) {

			// Check if sender requests an ack back.
			if (mGetRequestAck(_msg)) {
				// Copy message
				_msgTmp = _msg;
				mSetRequestAck(_msgTmp,
				               false); // Reply without ack flag (otherwise we would end up in an eternal loop)
				mSetAck(_msgTmp, true);
				_msgTmp.sender = getNodeId();
				_msgTmp.destination = _msg.sender;
				gatewayTransportSend(_msgTmp);
			}
			if (mGetCommand(_msg) == C_INTERNAL) {
				if (_msg.type == I_VERSION) {
					// Request for version. Create the response
					gatewayTransportSend(buildGw(_msgTmp, I_VERSION).set(MYSENSORS_LIBRARY_VERSION));
#ifdef MY_INCLUSION_MODE_FEATURE
				} else if (_msg.type == I_INCLUSION_MODE) {
					// Request to change inclusion mode
					inclusionModeSet(atoi(_msg.data) == 1);
#endif
				} else {
					(void)_processInternalCoreMessage();
				}
			} else {
				// Call incoming message callback if available
				if (receive) {
					receive(_msg);
				}
			}
		} else {
#if defined(MY_SENSOR_NETWORK)
			transportSendRoute(_msg);
#endif
		}
	}
}
#endif
