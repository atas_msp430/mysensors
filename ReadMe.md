MySensors v2.3.1 library changed to work on MSP430G2553 microcontroller
with NRF24L01+ as transport.
-NODE - work, takes 8Kb out of 16Kb flash and 232 byte out of 512 RAM
- Gateway Serial - not tested, in the process. (In earlier versions it worked, but there were many changes later that could affect.)
 now i use Orange Pi + NRF24 as gateway.
- Repeater mode not tested.


NRF24: MOSI - P1.7 MISO - P1.6 CLK -P1.5 CSN - P2.4 CE - P2.3 IRQ - P2.5 defined in nrf_userconfig.h
debug output to soft_serial (bitbang) TX-2.0 RX-2.1 (115200)
serial GW  RX-P1.1 TX-P1.2 (115200)

- node.cpp - user program which is similar to arduino project.
	Node with ID=5 (on GW inclusion mode must be turned ON)
	Thith two sensors - LED & Temperature.
	 - LED - may be switched by button on launchpad. state send/receive from controller
	 - Temperature - send every 3 sec to controller 
- some useful defines for MyConfig.h i define in MyConfigAddon.h 
- work on 16Mhz
- IRQ of RF24 now work. (implemented on Port 2)

- launchpad button on on P1.3 work with interrupt.
- HW UART and SPI uses interrupt and Low power mode 0 (LPM0) to sended/receive data. 
- Watchdog disabled.
- hwSleep use LPM0 and watchdog timer interrupt to wake
- hwCPUVoltage - implemeted, not tested.
- hwMillis use TIMER0_A0 interrupt.
- Circular buffer - work.	
	MY_RX_MESSAGE_BUFFER_SIZE value MUST be power of two (2,4,8...256). 
	this is due to optimization (mod % changed to using bit mask) 
	mask for index which prevents overflow. if size=4 what corresponds to b0100 then mask=size-1 = 3 what corresponds to b0011
	so if Index greater than 3 (size=4 and idx values= 0,1,2,3) and if apply a mask, the index will never be exceeded. -> 5 & 3 = 1 (b0101 & b0011 = b0001) 
	
- RF24_DEBUG() outputs to debug UART the name of the function in which it is lunched in addition to predefined debug messages of MySensors.
- serial debug messages may be highlighted in colors. 
	example -  ("Start demo sensor, press " GRN "SW1." RST "\r\n") - prints "SW1" in green color.  
- MyMessage::getString improved to use less memory	 
 

not work:
- EEPROM - cant save config
- Encription
- LEDs - not tested.
- interrupt callback in sleep/wait function.
- OTA Firmware/ Logging
- Signing
