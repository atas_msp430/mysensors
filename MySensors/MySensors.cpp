/*
 * MySensors.cpp
 *
 *  Created on: 9 ���. 2019 �.
 *      Author: Atas
 */
#include "MySensors/MySensors.h"

void main(void)
{
    _begin(); // Startup MySensors library
    for(;;) {
        _process();  // Process incoming data
        if (loop) {
            loop(); // Call sketch loop
        }
        if (serialEventRun) {
            serialEventRun();
        }
    }

}
