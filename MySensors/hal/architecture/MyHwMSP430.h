/*
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2018 Sensnology AB
 * Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 */

#ifndef MyHwMSP430_h
#define MyHwMSP430_h
//#include <libmaple/iwdg.h>
//#include <itoa.h>
#include <stddef.h>
#include "MySensors/MyConfig.h"

#ifdef MY_GATEWAY_SERIAL
#include "UART.h"
#endif

//#include <UART.h>
//#include "MySensors/MySensors.h"

extern volatile unsigned char flagsIRQ;

#define BUTTON      BIT3


#define flagKeyPressed  BUTTON

//#include <EEPROM.h>
//#ifdef __cplusplus
////#include <Arduino.h>
//#endif

//#define CRYPTO_LITTLE_ENDIAN


//#ifndef MY_STM32F1_TEMPERATURE_OFFSET
//#define MY_STM32F1_TEMPERATURE_OFFSET (0.0f)
//#endif

//#ifndef MY_STM32F1_TEMPERATURE_GAIN
//#define MY_STM32F1_TEMPERATURE_GAIN (1.0f)
//#endif

#define WD_LP0_100us    {WDTCTL = WDT_MDLY_0_064;IE1 |= WDTIE;LPM0;}
#define WD_LP0_550us    {WDTCTL = WDT_MDLY_0_5;IE1 |= WDTIE;LPM0;}
#define WD_LP3_6ms      {WDTCTL = WDT_ADLY_1_9;IE1 |= WDTIE;LPM3;}
#define WD_LP3_45ms     {WDTCTL = WDT_ADLY_16;IE1 |= WDTIE;LPM3;}
#define WD_LP3_730ms    {WDTCTL = WDT_ADLY_250;IE1 |= WDTIE;LPM3;}
#define WD_LP3_1s       {WDTCTL = WDT_ADLY_250;BCSCTL1 |= DIVA_1;IE1 |= WDTIE;LPM3;BCSCTL1 |= DIVA_0;}
#define WD_LP3_3s       {WDTCTL = WDT_ADLY_1000;IE1 |= WDTIE;LPM3;}
#define WD_LP3_5s       {WDTCTL = WDT_ADLY_1000;BCSCTL1 |= DIVA_1;IE1 |= WDTIE;LPM3;BCSCTL1 |= DIVA_0;} //32768 / (12000/2)
#define WD_LP3_10s      {WDTCTL = WDT_ADLY_1000;BCSCTL1 |= DIVA_2;IE1 |= WDTIE;LPM3;BCSCTL1 |= DIVA_0;} //32768 / (12000/4)
#define WD_LP3_21s      {WDTCTL = WDT_ADLY_1000;BCSCTL1 |= DIVA_3;IE1 |= WDTIE;LPM3;BCSCTL1 |= DIVA_0;} //32768 / (12000/8)

/* WDT is clocked by fACLK (assumed 12KHz) */
#define WDT_ADLY_2730       (WDTPW+WDTTMSEL+WDTCNTCL+WDTSSEL)                 /* 1/32768 2730 ms  " */
#define WDT_ADLY_682        (WDTPW+WDTTMSEL+WDTCNTCL+WDTSSEL+WDTIS0)          /* 1/8192 682.6 ms   " */
#define WDT_ADLY_42_6       (WDTPW+WDTTMSEL+WDTCNTCL+WDTSSEL+WDTIS1)          /* 1/512 42.6 ms    " */
#define WDT_ADLY_5_3        (WDTPW+WDTTMSEL+WDTCNTCL+WDTSSEL+WDTIS1+WDTIS0)   /* 1/64 5.3 ms   " */

#define WD_sec(s)   (s * (unsigned int) (12000 / 64))   //convert seconds to WD ticks ( ACLK/64=WD Divider)
#define WD_mSec(s)  (s * (unsigned int) (12000 / 64))   //convert seconds to WD ticks ( ACLK/64=WD Divider)



extern char _serialInputString[];
extern volatile unsigned char _serialInputPos;
extern volatile bool _serialMessageNeedProcess;


extern void serialEventRun(void) __attribute__((weak));
bool hwInit(void);
void hwRandomNumberInit(void);
void hwReadConfigBlock(void *buf, void *addr, size_t length);
void hwWriteConfigBlock(void *buf, void *addr, size_t length);
void hwWriteConfig(const int addr, unsigned char value);
unsigned char hwReadConfig(const int addr);
void hwWatchdogReset();
extern unsigned long hwMillis(void);
void hwReboot();
/**
 * @def MY_HWID_PADDING_BYTE
 * @brief HwID padding byte
 */
#define MY_HWID_PADDING_BYTE	(0xAAu)

/**
 * @def MY_HW_HAS_GETENTROPY
 * @brief Define this, if hwGetentropy is implemented
 *
 * ssize_t hwGetentropy(void *__buffer, size_t __length);
 */
//#define MY_HW_HAS_GETENTROPY

/// @brief unique ID
typedef unsigned char unique_id_t[16];

/**
 * Sleep for a defined time, using minimum power.
 * @param ms         Time to sleep, in [ms].
 * @return MY_WAKE_UP_BY_TIMER.
 */
signed char hwSleep(unsigned long ms);

/**
 * Sleep for a defined time, using minimum power, or until woken by interrupt.
 * @param interrupt  Interrupt number, which can wake the mcu from sleep.
 * @param mode       Interrupt mode, as passed to attachInterrupt.
 * @param ms         Time to sleep, in [ms].
 * @return MY_WAKE_UP_BY_TIMER when woken by timer, or interrupt number when woken by interrupt.
 */
signed char hwSleep(unsigned char interrupt1, unsigned char mode, unsigned long ms);

/**
 * Sleep for a defined time, using minimum power, or until woken by one of the interrupts.
 * @param interrupt1  Interrupt1 number, which can wake the mcu from sleep.
 * @param mode1       Interrupt1 mode, as passed to attachInterrupt.
 * @param interrupt2  Interrupt2 number, which can wake the mcu from sleep.
 * @param mode2       Interrupt2 mode, as passed to attachInterrupt.
 * @param ms          Time to sleep, in [ms].
 * @return MY_WAKE_UP_BY_TIMER when woken by timer, or interrupt number when woken by interrupt.
 */
signed char hwSleep(unsigned char interrupt1, unsigned char mode1, unsigned char interrupt2, unsigned char mode2, unsigned long ms);

/**
* Retrieve unique hardware ID
* @param uniqueID unique ID
* @return True if unique ID successfully retrieved
*/
//bool hwUniqueID(unique_id_t *uniqueID);

/**
 * CPU voltage
 * @return CPU voltage in mV
 */
unsigned int hwCPUVoltage();

/**
 * CPU frequency
 * @return CPU frequency in 1/10Mhz
 */
unsigned int hwCPUFrequency();

/**
 * CPU temperature (if available)
 * Adjust calibration parameters via MY_<ARCH>_TEMPERATURE_OFFSET and MY_<ARCH>_TEMPERATURE_GAIN
 * @return CPU temperature in �C, -127 if not available
 */
signed char hwCPUTemperature(void);

/**
 * Report free memory (if function available)
 * @return free memory in bytes
 */
unsigned int hwFreeMem(void);

#if defined(DEBUG_OUTPUT_ENABLED)
void hwDebugPrint(const char *fmt, ... );
#endif

/**
 * @def MY_CRITICAL_SECTION
 * @brief Creates a block of code that is guaranteed to be executed atomically.
 * Upon entering the block all interrupts are disabled, and re-enabled upon
 * exiting the block from any exit path.
 * A typical example that requires atomic access is a 16 (or more) bit variable
 * that is shared between the main execution path and an ISR, on an 8-bit
 * platform (e.g AVR):
 * @code
 * volatile unsigned int val = 0;
 *
 * void interrupHandler()
 * {
 *   val = ~val;
 * }
 *
 * void loop()
 * {
 *   unsigned int copy_val;
 *   MY_CRITICAL_SECTION
 *   {
 *     copy_val = val;
 *   }
 * }
 * @endcode
 * All code within the MY_CRITICAL_SECTION block will be protected from being
 * interrupted during execution.
 */


#endif
