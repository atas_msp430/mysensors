#ifndef UART_H_
#define UART_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <msp430.h>
#if !defined(__MSP430_HAS_USCI__)
 #error "This code written for the msp430g2553"
#endif
#define RXD             BIT1
#define TXD             BIT2
#define UART_BUF_SIZE	(32u)
//#include <stdarg.h>

#define _UART_RATE (115200)

#define mhz ((1000000.00)*BSP_CONFIG_CLOCK_MHZ_SELECT)
#define boud _UART_RATE
#define mhz_div_boud (mhz/boud)

#define _UCBRW (int)mhz_div_boud // word 16 bit
#define _UCBR0 (_UCBRW & 0xFF) //keep low byte
#define _UCBR1 ((_UCBRW & 0xFF00)>>8)

#define _UCBRSx (int)((mhz_div_boud - _UCBRW)*8+0.7)




#define USCI_INPUT_CLK      (16000000UL)  // in Hz
#define USCI_BAUD_RATE      (115200)
#define USCI_DIV_INT              (USCI_INPUT_CLK/USCI_BAUD_RATE)
#define USCI_BR0_VAL              (USCI_DIV_INT & 0x00FF)
#define USCI_BR1_VAL              ((USCI_DIV_INT >> 8) & 0xFF)

#define USCI_DIV_FRAC_NUMERATOR   (USCI_INPUT_CLK - (USCI_DIV_INT*USCI_BAUD_RATE))
#define USCI_DIV_FRAC_NUM_X_8     (USCI_DIV_FRAC_NUMERATOR*8)
#define USCI_DIV_FRAC_X_8         (USCI_DIV_FRAC_NUM_X_8/USCI_BAUD_RATE)

#if (((USCI_DIV_FRAC_NUM_X_8-(USCI_DIV_FRAC_X_8*USCI_BAUD_RATE))*10)/USCI_BAUD_RATE < 5)
#define USCI_BRS_VAL              (USCI_DIV_FRAC_X_8<< 1)
#else
#define USCI_BRS_VAL              ((USCI_DIV_FRAC_X_8+1)<< 1)
#endif




//#if (BSP_CONFIG_CLOCK_MHZ_SELECT==1)
//    #define baud_rate_20_bits  (((BSP_CONFIG_CLOCK_MHZ_SELECT * 1000000) + (_UART_RATE >> 1)) / _UART_RATE)
//#else
//    #define baud_rate_20_bits  (((BSP_CONFIG_CLOCK_MHZ_SELECT * 1000000) + (_UART_RATE >> 1)) / _UART_RATE)
//#endif

//http://processors.wiki.ti.com/index.php/USCI_UART_Baud_Rate_Gen_Mode_Selection
//  ---Low Frequency Baud-Rate Mode Setting (UCOS16=0)
//For fBRCLK=1MHz, BR=9600: N=1000000/9600 = 104,16666667
//UCBRx = INT(N) = 104
//UCBRSx = round (0,16666667 * 8) = round (1,33333333) = 1

//  ---Oversampling Baud-Rate Mode Setting (UCOS16=1)
//For fBRCLK=4MHz, BR=9600: N/16=4000000/9600/16 = 26,041666666666666666666666666667
//UCBRx = INT(N/16) = 26
//UCBRFx = round (0,041666666666666666666666666667 * 16) = round (0,66666666666666666666666666666667) = 1

//----------
//void uart_rx_isr(unsigned char);
//void uart_set_rx_isr_ptr(void (*isr_ptr)(unsigned char));
//void (*uart_rx_isr_ptr)(unsigned char);
//-----------------



void UART_init(void);
void uart_putc(unsigned char i);
void uart_puts(const char * tx_data);
void UART_TX(const unsigned char * tx_data,const unsigned char len);
//void UART_IN_INIT ();
//unsigned char UART_read(unsigned char * buf);
//void UART_TX_HEX(unsigned char * tx_data, unsigned char len);

#ifdef __cplusplus
}
#endif
#endif
