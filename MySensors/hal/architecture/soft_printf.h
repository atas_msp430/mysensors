#ifndef SOFTPRINT_H_
#define SOFTPRINT_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <msp430.h>
#include <stdarg.h>

#define colorReset "\u001b[0m"
#define colorRed "\u001b[31m"
#define colorGreen "\u001b[32m"
#define colorYellow "\u001b[33m"
#define colorBlue "\u001b[34m"
#define colorMagenta "\u001b[35m"
#define colorCyan "\u001b[36m"
#define colorWhite "\u001b[37m"
#define colorBlack "\u001b[30m"

#define RST colorReset
#define RED colorRed
#define GRN colorGreen
#define YEL colorYellow
#define BLU colorBlue
#define MGT colorMagenta
#define CYN colorCyan
#define WHT colorWhite
#define BLK colorBlack

#define hex(m) hexChar[m & 0x0F]
extern const unsigned char hexChar[];

static void xtoa(unsigned long x, const unsigned long *dp);
static void puth(unsigned n);
void soft_printf(const char * format, ...);
void snprintf_P(char * const s, const char *format, ...);
unsigned char* strAtoI(unsigned char * str, unsigned long value, unsigned char base);

#ifdef __cplusplus
}
#endif
#endif
