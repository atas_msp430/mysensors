#ifdef __cplusplus
extern "C" {
#endif

#include "UART.h"

//void (*uart_rx_isr_ptr)(unsigned char);
////-----------------
const unsigned char * uart_tx_buf;
unsigned char uart_tx_buf_len;
//unsigned char uart_rx_buf[UART_BUF_SIZE];
//unsigned char uart_rx_buf_idx;


//===============================================================================================================================
void UART_init(void)
{

P1DIR |= TXD;
P1SEL |= RXD | TXD ;               // P1.1 = RXD, P1.2=TXD
P1SEL2 |= RXD | TXD ;              //



//    UCA0CTL0  = 0;          // No parity, LSB first, 8 bits, one stop bit, UART (async)
//    BCSCTL3  |= LFXT1S_2;   // set low freq clock as vlo
//
//

//
//    UCA0BR0 = _UCBR0;
//    UCA0BR1 = _UCBR1;
//    UCA0MCTL = _UCBRSx<<1;


    // setup USCI UART registers
//    UCA0CTL1 |= UCSSEL_2 + UCSWRST;
    UCA0CTL1 |= UCSWRST;    // USCI reset and hold
    UCA0CTL1 |= UCSSEL_2;
    UCA0BR0 = USCI_BR0_VAL;
    UCA0BR1 = USCI_BR1_VAL;
    UCA0MCTL = USCI_BRS_VAL;
//    UCA0CTL1 &= ~UCSWRST;


//----------

    UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**


//IE2 |= UCA0RXIE;                          // Enable USCI_A0 RX interrupt
//IE2 |= UCA0TXIE; // Enable transmit interrupt
    IE2 |= UCA0RXIE;
//    __enable_interrupt();
//uart_rx_isr_ptr =0L;
}
//===============================================================================================================================
void uart_putc(unsigned char i)
{
	while ((UCA0STAT & UCBUSY)); // Wait if line TX/RX module is busy with data
	UCA0TXBUF = i; // Send out element i of tx_data array on UART bus
}
//===============================================================================================================================
void uart_puts(const char * tx_data)
//void UART_TX(char * tx_data) // Define a function which accepts a character pointer to an array
{
    unsigned int i=0;
    while(tx_data[i]) // Increment through array, look for null pointer (0) at end of string
    {
        while ((UCA0STAT & UCBUSY)); // Wait if line TX/RX module is busy with data
        UCA0TXBUF = tx_data[i]; // Send out element i of tx_data array on UART bus
        i++; // Increment variable for array address
    }
}
//===============================================================================================================================
void UART_TX(const unsigned char * tx_data, const unsigned char len)
{
	uart_tx_buf = tx_data;
	uart_tx_buf_len = len;
	IFG2 &= ~(UCA0TXIFG);
	IE2 |= UCA0TXIE;						// Enable USCI_A0 RX interrupt
	__enable_interrupt();
	UCA0TXBUF = * uart_tx_buf++;
    __bis_SR_register(LPM0_bits);
}
//===============================================================================================================================
#pragma vector = USCIAB0TX_VECTOR
__interrupt void USCI0TX_ISR(void)
{
	if (!--uart_tx_buf_len){
		IE2 &= ~UCA0TXIE; 						// Disable USCI_A0 TX interrupt
//		IFG2 &= ~(UCA0TXIFG);
		__bic_SR_register_on_exit(LPM3_bits);
	}
	else{
			UCA0TXBUF = * uart_tx_buf++;
	}
}
//===============================================================================================================================

//void UART_IN_INIT ()
//{
////    uart_rx_isr_ptr = isr_ptr;
////    if (uart_rx_isr_ptr == 0L) return;
////	uart_rx_buf_idx = 0;
//	IE2 |= UCA0RXIE;                          // Enable USCI_A0 RX interrupt
////	__bis_SR_register(LPM0_bits+GIE);
//	__enable_interrupt();
////	__bis_SR_register(GIE);
//
//}
//------------------------------------

//===============================================================================================================================



//===============================================================================================================================
//unsigned char UART_read(unsigned char * buf)
//{
//	unsigned char i=0;
//	unsigned char b;
//	while (1){
//		while(!(IFG2 & UCA0RXIFG));
//		b = UCA0RXBUF;
//		if ((b == 0x0d) || (b==0x0a)){
//			*buf = 0;
////			putc(0x0d);putc(0x0a);
//			break;
//		}
//		UCA0TXBUF = b;
//		while(!(IFG2 & UCA0TXIFG)); // Wait until UART is done
//		i++;
//		*buf++ = b;
//
//	}
//	return i;
//}
//===============================================================================================================================
//void UART_TX_HEX(unsigned char * tx_data, unsigned char len)
//{
//	unsigned char i;
//	for (i=len;i>0;i--){
////		printf("%h ",*tx_data++);
//	}
//}

#ifdef __cplusplus
}
#endif
