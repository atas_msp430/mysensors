/*
* The MySensors Arduino library handles the wireless radio link and protocol
* between your home built sensors/actuators and HA controller of choice.
* The sensors forms a self healing radio network with optional repeaters. Each
* repeater and gateway builds a routing tables in EEPROM which keeps track of the
* network topology allowing messages to be routed to nodes.
*
* Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
* Copyright (C) 2013-2018 Sensnology AB
* Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
*
* Documentation: http://www.mysensors.org
* Support Forum: http://forum.mysensors.org
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* Based on maniacbug's RF24 library, copyright (C) 2011 J. Coliz <maniacbug@ymail.com>
* RF24 driver refactored and optimized for speed and size, copyright (C) 2017 Olivier Mauti <olivier@mysensors.org>
*/

#include "RF24.h"
#include "soft_printf.h"

// debug output
#if defined(MY_DEBUG_VERBOSE_RF24)
#define RF24_DEBUG(x,...)	LOG_FUNC(x, ##__VA_ARGS__)	//!< DEBUG
#else
#define RF24_DEBUG(x,...)	//!< DEBUG null
#endif

static unsigned char RF24_BASE_ID[MY_RF24_ADDR_WIDTH] = { MY_RF24_BASE_RADIO_ID };
//static const unsigned char RF24_BASE_ID[MY_RF24_ADDR_WIDTH] = { MY_RF24_BASE_RADIO_ID };
static unsigned char RF24_NODE_ADDRESS = RF24_BROADCAST_ADDRESS;

#if defined(MY_RX_MESSAGE_BUFFER_FEATURE)
static RF24_receiveCallbackType RF24_receiveCallback = nullptr;
#endif


unsigned char RF24_readByteRegister(unsigned char addr)
{
    unsigned int i;

    CSN_EN;
    i = spi_transfer16(RF24_CMD_NOP | ((addr & RF24_REGISTER_MASK) << 8));

    CSN_DIS;
    return (unsigned char) (i & 0x00FF);
}

unsigned char RF24_writeByteRegister(unsigned char addr, unsigned char data)
{
    unsigned int i;
    CSN_EN;
    i = spi_transfer16( (data & 0x00FF) | (((addr & RF24_REGISTER_MASK) | RF24_CMD_WRITE_REGISTER) << 8) );
    CSN_DIS;
    return (unsigned char) ((i & 0xFF00) >> 8);
}

unsigned char RF24_spiMultiByteTransfer(const unsigned char cmd, unsigned char *buf, unsigned char len, const bool readMode)
{
	unsigned char status;
	unsigned char *current = buf;
	CSN_EN;
	// timing
//	delayMicroseconds(10);

	status = spi_transfer(cmd);
	while ( len-- ) {
		if (readMode) {
			status = spi_transfer(RF24_CMD_NOP);
			if (buf != nullptr) {
				*current++ = status;
			}
		} else {
			status = spi_transfer(*current++);
		}
	}
	CSN_DIS;
	// timing
//	delayMicroseconds(10);
	return status;
}

unsigned char RF24_spiByteTransfer(const unsigned char cmd)
{
    unsigned char rf_status;
    CSN_EN;
    rf_status = spi_transfer(cmd);
    CSN_DIS;
    return rf_status;
}
unsigned char RF24_RAW_writeByteRegister(const unsigned char cmd, unsigned char value)
{
    RF24_DEBUG(PSTR("RF24:WBR:REG=0x%h,VAL=0x%h\r\n"), cmd & RF24_REGISTER_MASK, value);
    //return RF24_spiMultiByteTransfer( cmd, &value, 1, false);
    unsigned char rf_status;
    unsigned int i;
    CSN_EN;
    i = spi_transfer16( (value & 0x00FF) | (((cmd & RF24_REGISTER_MASK) | RF24_CMD_WRITE_REGISTER) << 8) );
    CSN_DIS;
    rf_status = (unsigned char) ((i & 0xFF00) >> 8);
    return rf_status;
}

void RF24_flushRX(void)
{
//	RF24_DEBUG(PSTR("RF24:FRX\n\r"));
    RF24_DEBUG();
	RF24_spiByteTransfer(RF24_CMD_FLUSH_RX);
}

void RF24_flushTX(void)
{
    RF24_DEBUG();
	RF24_spiByteTransfer(RF24_CMD_FLUSH_TX);
}

unsigned char RF24_getStatus(void)
{
    unsigned char status =RF24_spiByteTransfer(RF24_CMD_NOP);
    RF24_DEBUG("Status=%b", status);
	return status;
}

unsigned char RF24_getFIFOStatus(void)
{
	return RF24_readByteRegister(RF24_REG_FIFO_STATUS);
}

void RF24_setChannel(const unsigned char channel)
{
	RF24_writeByteRegister(RF24_REG_RF_CH,channel);
}

void RF24_setRetries(const unsigned char retransmitDelay, const unsigned char retransmitCount)
{
	RF24_writeByteRegister(RF24_REG_SETUP_RETR, retransmitDelay << RF24_ARD | retransmitCount << RF24_ARC);
}

void RF24_setAddressWidth(const unsigned char addressWidth)
{
    RF24_DEBUG("width=%u", addressWidth);
	RF24_writeByteRegister(RF24_REG_SETUP_AW, addressWidth - 2);
}

void RF24_setRFSetup(const unsigned char RFsetup)
{
    RF24_DEBUG("RF Setup=%b", RFsetup);
	RF24_writeByteRegister(RF24_REG_RF_SETUP, RFsetup);
}

void RF24_setFeature(const unsigned char feature)
{
    RF24_DEBUG("Feature=%b", feature);
	RF24_writeByteRegister(RF24_REG_FEATURE, feature);
	unsigned char checkFeature =RF24_getFeature();
	if ( checkFeature != feature) {
	    RF24_DEBUG(RED"ERR"RST" Feature=%b Check=%b", feature, checkFeature);
		// toggle features (necessary on some clones and non-P versions)
		RF24_enableFeatures();
		RF24_writeByteRegister(RF24_REG_FEATURE, feature);
	}
}
void RF24_enableFeatures(void)
{
    RF24_RAW_writeByteRegister(RF24_CMD_ACTIVATE, 0x73);
}

unsigned char RF24_getFeature(void)
{
	return RF24_readByteRegister(RF24_REG_FEATURE);
}

void RF24_setPipe(const unsigned char pipe)
{
    RF24_DEBUG("pipe=%b",pipe);
	RF24_writeByteRegister(RF24_REG_EN_RXADDR, pipe);
}

void RF24_setAutoACK(const unsigned char pipe)
{
    RF24_DEBUG("pipe=%b",pipe);
	RF24_writeByteRegister(RF24_REG_EN_AA, pipe);
}

void RF24_setDynamicPayload(const unsigned char pipe)
{
	RF24_writeByteRegister(RF24_REG_DYNPD, pipe);
}

void RF24_setRFConfiguration(const unsigned char configuration)
{
    RF24_DEBUG("=%b RX_IRQ=%u TX_IRQ=%u MAX_RT=%u PWR=%s PRIM_RX=%s",
             configuration,
             (configuration & RF24_MASK_RX_DR )==0,
             (configuration & RF24_MASK_TX_DS )==0,
             (configuration & RF24_MASK_MAX_RT )==0,
             (configuration & RF24_PWR_UP )==0 ? "DOWN":"UP",
             (configuration & RF24_PRIM_RX )==0 ? "TX":"RX"
             );
	RF24_writeByteRegister(RF24_REG_NRF_CONFIG, configuration);
}

void RF24_setPipeAddress(const unsigned char pipe, unsigned char *address, const unsigned char addressWidth)
{
    RF24_DEBUG("pipe=%h addr=%h addr w=%u",pipe , address[0] , addressWidth);
	RF24_writeMultiByteRegister(pipe, address, addressWidth);
}

void RF24_setPipeLSB(const unsigned char pipe, const unsigned char LSB)
{
    RF24_DEBUG("=%h LSB=%h",pipe, LSB);
	RF24_writeByteRegister(pipe, LSB);
}

unsigned char RF24_getObserveTX(void)
{
	return RF24_readByteRegister(RF24_REG_OBSERVE_TX);
}

void RF24_setStatus(const unsigned char status)
{
    RF24_DEBUG("Status=%b  ",status);
	RF24_writeByteRegister(RF24_REG_STATUS, status);
}

void RF24_openWritingPipe(const unsigned char recipient)
{
    RF24_DEBUG("=%u\r\n", recipient); // open writing pipe
	// only write LSB of RX0 and TX pipe
	RF24_setPipeLSB(RF24_REG_RX_ADDR_P0, recipient);
	RF24_setPipeLSB(RF24_REG_TX_ADDR, recipient);
}

void RF24_startListening(void)
{
//	RF24_DEBUG(PSTR("RF24:STL\n\r"));	// start listening
    RF24_DEBUG();
	// toggle PRX
	RF24_setRFConfiguration(RF24_REG_NRF_CONFIGURATION | RF24_PWR_UP | RF24_PRIM_RX);

	// all RX pipe addresses must be unique, therefore skip if node ID is RF24_BROADCAST_ADDRESS
	if(RF24_NODE_ADDRESS!= RF24_BROADCAST_ADDRESS) {
		RF24_setPipeLSB(RF24_REG_RX_ADDR_P0, RF24_NODE_ADDRESS);
	}
	// start listening
	CE_EN;
}

void RF24_stopListening(void)
{
//	RF24_DEBUG(PSTR("RF24:SPL\n\r"));	// stop listening
    RF24_DEBUG();
	CE_DIS;
	// timing
	delayMicroseconds(130);
	RF24_setRFConfiguration(RF24_REG_NRF_CONFIGURATION | RF24_PWR_UP );
	// timing
	delayMicroseconds(100);
}

void RF24_powerDown(void)
{
#if defined(MY_RF24_POWER_PIN)
	hwDigitalWrite(MY_RF24_POWER_PIN, LOW);
#endif
}

void RF24_powerUp(void)
{
#if defined(MY_RF24_POWER_PIN)
	hwDigitalWrite(MY_RF24_POWER_PIN, HIGH);
	delay(RF24_POWERUP_DELAY_MS);	// allow VCC to settle
#endif
}
void RF24_sleep(void)
{
//	RF24_DEBUG(PSTR("RF24:SLP\n\r")); // put radio to sleep
    RF24_DEBUG();
	CE_DIS;
	RF24_setRFConfiguration(RF24_REG_NRF_CONFIGURATION);
}

void RF24_standBy(void)
{
//	RF24_DEBUG(PSTR("RF24:SBY\n\r")); // put radio to standby
    RF24_DEBUG();
	CE_DIS;
	RF24_setRFConfiguration(RF24_REG_NRF_CONFIGURATION | RF24_PWR_UP);
	// There must be a delay of up to 4.5ms after the nRF24L01+ leaves power down mode before the CE is set high.
	delayMicroseconds(6);
//	delay(4500);
}


bool RF24_sendMessage(const unsigned char recipient, const void *buf, const unsigned char len,
                            const bool noACK)
{
//    LOG;
	unsigned char RF24_status;
	RF24_stopListening();
	RF24_openWritingPipe( recipient );
	RF24_DEBUG("TO=%u,LEN=%u\r\n",recipient,len); // send message
	// flush TX FIFO
	RF24_flushTX();
	// this command is affected in clones (e.g. Si24R1):  flipped NoACK bit when using W_TX_PAYLOAD_NO_ACK / W_TX_PAYLOAD
	// AutoACK is disabled on the broadcasting pipe - NO_ACK prevents resending
	RF24_spiMultiByteTransfer((recipient == RF24_BROADCAST_ADDRESS ||
	                           noACK) ? RF24_CMD_WRITE_TX_PAYLOAD_NO_ACK :
	                          RF24_CMD_WRITE_TX_PAYLOAD, (unsigned char *) buf, len, false );
	// go, TX starts after ~10us, CE high also enables PA+LNA on supported HW
	CE_EN;
	// timeout counter to detect HW issues
	unsigned int timeout = 0xFFFF;
	do {
		RF24_status = RF24_getStatus();
	} while  (!(RF24_status & ( (RF24_MAX_RT) | (RF24_TX_DS) )) && timeout--);
	// timeout value after successful TX on 16Mhz AVR ~ 65500, i.e. msg is transmitted after ~36 loop cycles
	CE_DIS;
	// reset interrupts
	RF24_setStatus((RF24_TX_DS) | (RF24_MAX_RT) );
	// Max retries exceeded
	if(RF24_status & (RF24_MAX_RT)) {
		// flush packet
		RF24_DEBUG(PSTR("!RF24:TXM:MAX_RT\n\r"));	// max retries, no ACK
		RF24_flushTX();
	}
	RF24_startListening();
	// true if message sent
	return ((RF24_status & RF24_TX_DS) || noACK);
}

unsigned char RF24_getDynamicPayloadSize(void)
{
	unsigned char result = RF24_spiMultiByteTransfer(RF24_CMD_READ_RX_PL_WID, nullptr, 1, true);
	RF24_DEBUG("Payload size=%u",result);
	// check if payload size invalid
	if(result > 32) {
		RF24_DEBUG(PSTR("!RF24:GDP:PYL INV\n\r")); // payload len invalid
		RF24_flushRX();
		result = 0;
	}
	return result;
}

bool RF24_isDataAvailable(void)
{
static volatile unsigned char prevFIFO = 0xFF;
unsigned char FIFO =RF24_getFIFOStatus();
if (FIFO != prevFIFO){
    RF24_DEBUG(GRN"FIFO"RST"=%b",FIFO);
    prevFIFO=FIFO;
}
	return (!(FIFO & BIT0) );
}


unsigned char RF24_readMessage(void *buf)
{
	const unsigned char len = RF24_getDynamicPayloadSize();
//	RF24_DEBUG(PSTR("RF24:RXM:LEN=%u\n\r"), len);	// read message
	RF24_DEBUG("len=%u",len);
	RF24_spiMultiByteTransfer(RF24_CMD_READ_RX_PAYLOAD,(unsigned char *)buf,len,true);
	// clear RX interrupt
	RF24_setStatus((RF24_RX_DR));
	return len;
}

void RF24_setNodeAddress(const unsigned char address)
{
    RF24_DEBUG("addr=%h",address);
	if(address!= RF24_BROADCAST_ADDRESS) {
		RF24_NODE_ADDRESS = address;
		// enable node pipe
		RF24_setPipe((1 << RF24_BROADCAST_PIPE) | 1<<RF24_NODE_PIPE);
		// enable autoACK on pipe 0
		RF24_setAutoACK(1<<RF24_NODE_PIPE);
	}
}

unsigned char RF24_getNodeID(void)
{
	return RF24_NODE_ADDRESS;
}

bool RF24_sanityCheck(void)
{
	// detect HW defect, configuration errors or interrupted SPI line, CE disconnect cannot be detected
	return (RF24_readByteRegister(RF24_REG_RF_SETUP) == RF24_RF_SETUP) && (RF24_readByteRegister(
	            RF24_REG_RF_CH) == MY_RF24_CHANNEL);
}
int RF24_getTxPowerLevel(void)
{
	// in dBm ((-6) *(3-((rfSet >> 1) & 3)))
    unsigned char rfSet = RF24_readByteRegister(RF24_REG_RF_SETUP);
    unsigned char lvl = (3-((rfSet >> 1) & 3));
	return (int) lvl;
}

unsigned char RF24_getTxPowerPercent(void)
{
	// report TX level in %, 0 (LOW) = 25%, 3 (MAX) = 100
	//const unsigned char result = 25 + (((RF24_readByteRegister(RF24_REG_RF_SETUP) >> 2) & 3) * 25); // = x*8+x*8+x*8+x
    unsigned char result = RF24_readByteRegister(RF24_REG_RF_SETUP);
    result = 25 + (result& BIT2 ? 50:0) + (result& BIT1 ? 25:0); //max = 25 + (bit2(1) =50) + (bit1(1) =25) = 100%
	return result;
}
bool RF24_setTxPowerLevel(const unsigned char newPowerLevel)
{
	const unsigned char registerContent = RF24_readByteRegister(RF24_REG_RF_SETUP);
	RF24_writeByteRegister(RF24_REG_RF_SETUP, (registerContent & 0xF9) | ((newPowerLevel & 3) << 1));
	RF24_DEBUG(PSTR("RF24:STX:LEVEL=%u\n\r"), newPowerLevel);
	return true;
}

bool RF24_setTxPowerPercent(const unsigned char newPowerPercent)
{
//	const unsigned char newPowerLevel = static_cast<unsigned char>(RF24_MIN_POWER_LEVEL + (RF24_MAX_POWER_LEVEL - RF24_MIN_POWER_LEVEL) * (newPowerPercent / 100.0f));
//	return RF24_setTxPowerLevel(newPowerLevel);
    return RF24_setTxPowerLevel(RF24_MAX_POWER_LEVEL);
}
int RF24_getSendingRSSI(void)
{
	// calculate pseudo-RSSI based on retransmission counter (ARC)
	// min -104dBm at 250kBps
	// Arbitrary definition: ARC 0 == -29, ARC 15 = -104
	return static_cast<int>(-29 - ( (RF24_getObserveTX() & 0xF) <<3));
}

void RF24_enableConstantCarrierWave(void)
{
	RF24_standBy();
	RF24_setRFSetup(RF24_RF_SETUP | (RF24_CONT_WAVE) | (RF24_PLL_LOCK) );
	CE_EN;
}

void RF24_disableConstantCarrierWave(void)
{
	CE_DIS;
	RF24_setRFSetup(RF24_RF_SETUP);
}

bool RF24_getReceivedPowerDetector(void)
{
	// nRF24L01+ only. nRF24L01 contains a carrier detect function (same register & bit) which works
	// slightly different and takes at least 128us to become active.
	return (RF24_readByteRegister(RF24_REG_RPD) & BIT0) != 0;
}

#if defined(MY_RX_MESSAGE_BUFFER_FEATURE)
void RF24_irqHandler(void)
{
	if (RF24_receiveCallback) {
#if defined(MY_GATEWAY_SERIAL) && !defined(__linux__)
		// Will stay for a while (several 100us) in this interrupt handler. Any interrupts from serial
		// rx coming in during our stay will not be handled and will cause characters to be lost.
		// As a workaround we re-enable interrupts to allow nested processing of other interrupts.
		// Our own handler is disconnected to prevent recursive calling of this handler.
//		detachInterrupt(digitalPinToInterrupt(RF24_IRQ_PIN));
//		interrupts();
#endif
		// Read FIFO until empty.
		// Procedure acc. to datasheet (pg. 63):
		// 1.Read payload, 2.Clear RX_DR IRQ, 3.Read FIFO_status, 4.Repeat when more data available.
		// Datasheet (ch. 8.5) states, that the nRF de-asserts IRQ after reading STATUS.


		// Start checking if RX-FIFO is not empty, as we might end up here from an interrupt
		// for a message we've already read.
		while (RF24_isDataAvailable()) {
			RF24_receiveCallback();		// Must call RF24_readMessage(), which will clear RX_DR IRQ !
		}


#if defined(MY_GATEWAY_SERIAL) && !defined(__linux__)
		// Restore our interrupt handler.
//		noInterrupts();
//		attachInterrupt(digitalPinToInterrupt(RF24_IRQ_PIN), RF24_irqHandler, FALLING);
#endif
	} else {
		// clear RX interrupt
		RF24_setStatus((RF24_RX_DR));
	}
}

void RF24_registerReceiveCallback(RF24_receiveCallbackType cb)
{
	MY_CRITICAL_SECTION {
		RF24_receiveCallback = cb;
	}
}
#endif

bool RF24_initialize(void)
{
	RF24_DEBUG();
	// Initialize pins & HW
	RF24_HW_init();
	RF24_IRQ_Setup();

    RF24_powerUp();
//#if defined(MY_RX_MESSAGE_BUFFER_FEATURE)
//    hwPinMode(RF24_IRQ_PIN,INPUT);
//#endif
    // power up and standby
    RF24_standBy();
    // set address width
    RF24_setAddressWidth(MY_RF24_ADDR_WIDTH);
    // auto retransmit delay 1500us, auto retransmit count 15
    RF24_setRetries(RF24_SET_ARD, RF24_SET_ARC);
    // set channel
    RF24_setChannel(MY_RF24_CHANNEL);
    // set data rate and pa level
    RF24_setRFSetup(RF24_RF_SETUP);
    // enable ACK payload and dynamic payload
    RF24_setFeature(RF24_FEATURE);
    // sanity check (this function is P/non-P independent)
    if (!RF24_sanityCheck()) {
        RF24_DEBUG(PSTR("!RF24:INIT:SANCHK " RED "FAIL\n" RST)); // sanity check failed, check wiring or replace module
        return false;
    }
    // enable broadcasting pipe
    RF24_setPipe(1<<RF24_BROADCAST_PIPE);
    // disable AA on all pipes, activate when node pipe set
    RF24_setAutoACK(0x00);
    // enable dynamic payloads on used pipes
    RF24_setDynamicPayload(1<<RF24_BROADCAST_PIPE | 1<<RF24_NODE_PIPE);//RF24_DPL_P0 + RF24_BROADCAST_PIPE | RF24_DPL_P0);
    // listen to broadcast pipe
    RF24_BASE_ID[0] = RF24_BROADCAST_ADDRESS;
    RF24_setPipeAddress(RF24_REG_RX_ADDR_P0 + RF24_BROADCAST_PIPE, RF24_BASE_ID,
                        RF24_BROADCAST_PIPE > 1 ? 1 : MY_RF24_ADDR_WIDTH);
    // pipe 0, set full address, later only LSB is updated
    RF24_setPipeAddress(RF24_REG_RX_ADDR_P0 + RF24_NODE_PIPE, RF24_BASE_ID, MY_RF24_ADDR_WIDTH);
    RF24_setPipeAddress(RF24_REG_TX_ADDR, RF24_BASE_ID, MY_RF24_ADDR_WIDTH);
    // reset FIFO
    RF24_flushRX();
    RF24_flushTX();
    // reset interrupts
    RF24_setStatus(RF24_TX_DS | RF24_MAX_RT | RF24_RX_DR);
//    LPM4;
    return true;
}
void RF24_IRQ_Setup()
{
    // Setup IRQ
        #if nrfIRQport == 1
            P1DIR &= ~RF24_IRQ_PIN;  // IRQ line is input
            P1OUT |= RF24_IRQ_PIN;   // Pull-up resistor enabled
            P1REN |= RF24_IRQ_PIN;
            P1IES |= RF24_IRQ_PIN;   // Trigger on falling-edge
            P1IFG &= ~RF24_IRQ_PIN;  // Clear any outstanding IRQ
            P1IE |= RF24_IRQ_PIN;    // Enable IRQ interrupt
        #elif nrfIRQport == 2
            P2DIR &= ~RF24_IRQ_PIN;  // IRQ line is input
            P2OUT |= RF24_IRQ_PIN;   // Pull-up resistor enabled
            P2REN |= RF24_IRQ_PIN;
            P2IES |= RF24_IRQ_PIN;   // Trigger on falling-edge
            P2IFG &= ~RF24_IRQ_PIN;  // Clear any outstanding IRQ
            P2IE |= RF24_IRQ_PIN;    // Enable IRQ interrupt
        #elif nrfIRQport == 3
            P3DIR &= ~RF24_IRQ_PIN;  // IRQ line is input
            P3OUT |= RF24_IRQ_PIN;   // Pull-up resistor enabled
            P3REN |= RF24_IRQ_PIN;
            P3IES |= RF24_IRQ_PIN;   // Trigger on falling-edge
            P3IFG &= ~RF24_IRQ_PIN;  // Clear any outstanding IRQ
            P3IE |= RF24_IRQ_PIN;    // Enable IRQ interrupt
        #elif nrfIRQport == 2
            P4DIR &= ~RF24_IRQ_PIN;  // IRQ line is input
            P4OUT |= RF24_IRQ_PIN;   // Pull-up resistor enabled
            P4REN |= RF24_IRQ_PIN;
            P4IES |= RF24_IRQ_PIN;   // Trigger on falling-edge
            P4IFG &= ~RF24_IRQ_PIN;  // Clear any outstanding IRQ
            P4IE |= RF24_IRQ_PIN;    // Enable IRQ interrupt
        #endif

}
void RF24_HW_init()
{
    // Setup SPI
    spi_init();
    _EINT();  // Enable interrupts (set GIE in SR)


    // Setup CSN/CE ports
    #if nrfCSNport == 1
        P1DIR |= RF24_CSN_PIN;
    #elif nrfCSNport == 2
        P2DIR |= RF24_CSN_PIN;
    #elif nrfCSNport == 3
        P3DIR |= RF24_CSN_PIN;
    #elif nrfCSNport == 4
        P3DIR |= RF24_CSN_PIN;
    #elif nrfCSNport == 5
        P3DIR |= RF24_CSN_PIN;
    #elif nrfCSNport == 6
        P3DIR |= RF24_CSN_PIN;
    #elif nrfCSNport == 7
        P3DIR |= RF24_CSN_PIN;
    #elif nrfCSNport == 8
        P3DIR |= RF24_CSN_PIN;
    #elif nrfCSNport == 9
        P3DIR |= RF24_CSN_PIN;
    #elif nrfCSNport == J
        PJDIR |= RF24_CSN_PIN;
    #endif
    CSN_DIS;

    #if nrfCEport == 1
        P1DIR |= RF24_CE_PIN;
    #elif nrfCEport == 2
        P2DIR |= RF24_CE_PIN;
    #elif nrfCEport == 3
        P3DIR |= RF24_CE_PIN;
    #elif nrfCEport == 4
        P3DIR |= RF24_CE_PIN;
    #elif nrfCEport == 5
        P3DIR |= RF24_CE_PIN;
    #elif nrfCEport == 6
        P3DIR |= RF24_CE_PIN;
    #elif nrfCEport == 7
        P3DIR |= RF24_CE_PIN;
    #elif nrfCEport == 8
        P3DIR |= RF24_CE_PIN;
    #elif nrfCEport == 9
        P3DIR |= RF24_CE_PIN;
    #elif nrfCEport == J
        PJDIR |= RF24_CE_PIN;
    #endif
    CE_DIS;

    /* Straw-man spi_transfer with no Chip Select lines enabled; this is to workaround errata bug USI5
     * on the MSP430G2452 and related (see http://www.ti.com/lit/er/slaz072/slaz072.pdf)
     * Shouldn't hurt anything since we expect no CS lines enabled by the user during this function's execution.
     */
    spi_transfer(RF24_CMD_NOP);

    // Wait 100ms for RF transceiver to initialize.
    unsigned char c = 20;
    for (; c; c--) {
        __delay_cycles(DELAY_CYCLES_5MS);
    }

}


/*================================================================
 *       -       -       Interrupt vectors       -       -
 *================================================================*/
//#if defined(MY_RX_MESSAGE_BUFFER_FEATURE)
// RF transceiver IRQ handling
#if   nrfIRQport == 2
  #ifdef __GNUC__
  __attribute__((interrupt(PORT2_VECTOR)))
  void P2_IRQ (void) {
  #else
  #pragma vector = PORT2_VECTOR
  __interrupt void P2_IRQ (void) {
  #endif
    if(P2IFG & RF24_IRQ_PIN) {
        P2IFG &= ~RF24_IRQ_PIN;   // Clear interrupt flag
        #if defined(MY_RX_MESSAGE_BUFFER_FEATURE)
        P2IE &= ~RF24_IRQ_PIN;
        __bis_SR_register(GIE);
        RF24_irqHandler();
        P2IE |= RF24_IRQ_PIN;
        #endif
        __bic_SR_register_on_exit(LPM4_bits);    // Wake up


    }
}

#elif nrfIRQport == 1
  #ifdef __GNUC__
  __attribute__((interrupt(PORT1_VECTOR)))
  void P1_IRQ (void) {
  #else
  #pragma vector = PORT1_VECTOR
  __interrupt void P1_IRQ (void) {
  #endif
    if(P1IFG & RF24_IRQ_PIN) {
        __bic_SR_register_on_exit(LPM4_bits);
        rf_irq |= RF24_IRQ_FLAGGED;
        P1IFG &= ~RF24_IRQ_PIN;
    }
}
//#endif
#endif
