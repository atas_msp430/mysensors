
#ifndef ITOA_H_
#define ITOA_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    unsigned long quot;
    unsigned char rem;
}divmod10_t;

static divmod10_t divmodu10(unsigned long n);
char * utoa(unsigned long value, char *buffer);
char * itoa(long value, char *buffer);

#ifdef __cplusplus
}
#endif
#endif /* ITOA_H_ */
