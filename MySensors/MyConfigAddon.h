#ifndef MyConfigAddon_h
#define MyConfigAddon_h
#include "soft_printf.h"
#define softTXD     BIT0
#define softRXD     BIT1


//#define MY_DEBUG
//#define MY_DEBUG_VERBOSE_RF24
//#define MY_SPECIAL_DEBUG

#define BSP_CONFIG_CLOCK_MHZ_SELECT (16)

#define MY_SPLASH_SCREEN_DISABLED
#define MY_BAUD_RATE (115200ul)

#define MY_RADIO_RF24

#define MY_NODE_ID (5)
//#define MY_REGISTRATION_DEFAULT true
//#define MY_GATEWAY_SERIAL
//#define MY_REPEATER_FEATURE
#define MY_RX_MESSAGE_BUFFER_FEATURE

//#define RF24_IRQ_PIN     (RF24_IRQ_PIN) //PORT2 //nrf_userconfug.h
//#define RF24_CSN_PIN      (RF24_CSN_PIN)
//#define RF24_CE_PIN      (RF24_CE_PIN)
#define SPI_HAS_TRANSACTION
#define MY_CRITICAL_SECTION
#define MY_SIGNAL_REPORT_ENABLED
#define MY_RAM_ROUTING_TABLE_ENABLED
#define MY_RF24_ADDR_WIDTH (5)

//#define MY_DEFAULT_TX_LED_PIN (7)
//#define MY_DEFAULT_ERR_LED_PIN (6)
//#define MY_DEFAULT_RX_LED_PIN (8)

#define delayMicroseconds(x) hwSleep(x)
#define delay(x) hwSleep(x)

//extern void soft_printf(const char *format, ...);

#define F(string_literal) (reinterpret_cast<const __FlashStringHelper*>(PSTR(string_literal)))

#define FV(x) (__FlashStringHelper*)(x)

//#define PRIu8 "u"
//#define PROGMEM  __attribute__((__progmem__))
#define PSTR(s) s

/// Macro to set a bit y in variable x
#define SETB(x,y)       (x |= (y))
/// Macro to reset a bit y in variable x
#define CLRB(x,y)       (x &= ~(y))
/// Macro to toggle a bit y in variable x
#define INVB(x,y)       (x ^= (y))

extern unsigned long hwMillis(void);

#define LOG_ENABLE

#if defined(LOG_ENABLE)
    #define _LOGCOLOR(C,x,...)  DEBUG_OUTPUT(C "%i \t %s\t ms=%l" colorReset "\t" x "\r\n",__LINE__, __func__, hwMillis(),##__VA_ARGS__)
    #define _LOG(C,...)  _LOGCOLOR(C ,##__VA_ARGS__)
    #define DLOG(...)  _LOGCOLOR(colorYellow,##__VA_ARGS__)
    #define LOG  _LOG(colorYellow,,)

    #define LOG_FUNC(x,...) DEBUG_OUTPUT("%s\t" x "\r\n", __func__,##__VA_ARGS__)
    #define LOG_RED(x,...)  DEBUG_OUTPUT(colorRed x colorReset, ##__VA_ARGS__)
    #define LOG_GREEN(x,...)  DEBUG_OUTPUT(colorGreen x colorReset, ##__VA_ARGS__)
#else
    #define _LOGCOLOR(C,x,...)
    #define _LOG(C,...)
    #define DLOG(...)
    #define LOG

    #define LOG_RED(x,...)
    #define LOG_GREEN(x,...)
#endif

#define LED1        BIT0
#define LED2        BIT6
#define LED1_SWITCH P1OUT ^= LED1
#define LED1_ON     P1OUT |= LED1
#define LED1_OFF    P1OUT &= ~LED1

#endif //MyCongigAddon
