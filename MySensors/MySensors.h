/*
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2018 Sensnology AB
 * Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 */

/**
 * @defgroup MySensorsgrp MySensors
 * @ingroup publics
 * @{
 * @brief The primary public API declaration for the MySensors library
 */

/**
 * @file MySensors.h
 *
 * @brief API declaration for MySensors
 *
 * Include this header into your sketch to include the MySensors library and harness the power of
 * all those sensors!
 */
#ifndef MySensors_h
#define MySensors_h

#ifdef __cplusplus
//#include <Arduino.h>
#endif
#include <msp430.h>
#include <stddef.h>

#include "MyConfig.h"
#ifdef DEBUG_OUTPUT_ENABLED
#include "soft_serial.h"
#include "soft_printf.h"
#endif

// OTA Debug, has to be defined before HAL
#if defined(MY_OTA_LOG_SENDER_FEATURE) || defined(MY_OTA_LOG_RECEIVER_FEATURE)
#include "core/MyOTALogging.h"
#endif

// HARDWARE
//#include "hal/architecture/MyHwHAL.h"
#include "MySensors/hal/architecture/MyHwMSP430.h"
#include "MySensors/hal/crypto/MyCryptoHAL.h"

// commonly used macros, sometimes missing in arch definitions
#if !defined(_BV)
#define _BV(x) (1<<(x))	//!< _BV
#endif

#if !defined(min) && !defined(__linux__)
#define min(a,b) ((a)<(b)?(a):(b)) //!< min
#endif

#if !defined(max) && !defined(__linux__)
#define max(a,b) ((a)>(b)?(a):(b)) //!< max
#endif

#if !defined(MIN)
#define MIN min //!< MIN
#endif

#if !defined(MAX)
#define MAX max //!< MAX
#endif

// OTA Debug second part, depends on HAL
#if defined(MY_OTA_LOG_SENDER_FEATURE) || defined(MY_OTA_LOG_RECEIVER_FEATURE)
#include "core/MyOTALogging.h"
#endif

#if defined(MY_LEDS_BLINKING_FEATURE)
#error MY_LEDS_BLINKING_FEATURE is now removed from MySensors core,\
define MY_DEFAULT_ERR_LED_PIN, MY_DEFAULT_TX_LED_PIN or\
MY_DEFAULT_RX_LED_PIN in your sketch instead to enable LEDs
#endif

#if defined(MY_DEFAULT_RX_LED_PIN) || defined(MY_DEFAULT_TX_LED_PIN) || defined(MY_DEFAULT_ERR_LED_PIN)
#include "core/MyLeds.h"
#else
#include "core/MyLeds.h"
#endif

#include "core/MyIndication.h"


// INCLUSION MODE
#if defined(MY_INCLUSION_MODE_FEATURE)
#include "core/MyInclusionMode.h"
#endif


// SIGNING
#include "core/MySigning.h"
#if defined(MY_SIGNING_FEATURE)
// SIGNING COMMON FUNCTIONS
#if defined(MY_SIGNING_ATSHA204) && defined(MY_SIGNING_SOFT)
#error Only one signing engine can be activated
#endif
#if defined(MY_SIGNING_ATSHA204) && defined(__linux__)
#error No support for ATSHA204 on this platform
#endif

#if defined(MY_SIGNING_ATSHA204)
#include "core/MySigningAtsha204.h"
#include "drivers/ATSHA204/ATSHA204.h"
#elif defined(MY_SIGNING_SOFT)
#include "core/MySigningAtsha204Soft.h"
#endif
#endif

// FLASH
#if defined(MY_OTA_FIRMWARE_FEATURE)
#ifndef MCUBOOT_PRESENT
#if defined(MY_OTA_USE_I2C_EEPROM)
#include "drivers/I2CEeprom/I2CEeprom.h"
#else
#include "drivers/SPIFlash/SPIFlash.h"
#endif
#endif
#include "core/MyOTAFirmwareUpdate.h"
#endif

// GATEWAY - TRANSPORT
#if defined(MY_CONTROLLER_IP_ADDRESS) || defined(MY_CONTROLLER_URL_ADDRESS)
#define MY_GATEWAY_CLIENT_MODE	//!< gateway client mode
#endif

#if defined(MY_USE_UDP) && !defined(MY_GATEWAY_CLIENT_MODE)
#error You must specify MY_CONTROLLER_IP_ADDRESS or MY_CONTROLLER_URL_ADDRESS for UDP
#endif

#if defined(MY_GATEWAY_MQTT_CLIENT)
#if defined(MY_SENSOR_NETWORK)
// We assume that a gateway having a radio also should act as repeater
#define MY_REPEATER_FEATURE
#endif

// GATEWAY - COMMON FUNCTIONS
// We support MQTT Client using W5100, ESP8266, GSM modems supported by TinyGSM library and Linux
#if !defined(MY_GATEWAY_CLIENT_MODE) && !defined(MY_GATEWAY_TINYGSM)
#error You must specify MY_CONTROLLER_IP_ADDRESS or MY_CONTROLLER_URL_ADDRESS
#endif

#if defined(MY_GATEWAY_TINYGSM) && !defined(MY_GATEWAY_MQTT_CLIENT)
// TinyGSM currently only supports MQTTClient mode.
#error MY_GATEWAY_TINYGSM only works with MY_GATEWAY_MQTT_CLIENT
#endif

#if !defined(MY_MQTT_PUBLISH_TOPIC_PREFIX)
#error You must specify a topic publish prefix MY_MQTT_PUBLISH_TOPIC_PREFIX for this MQTT client
#endif

#if !defined(MY_MQTT_SUBSCRIBE_TOPIC_PREFIX)
#error You must specify a topic subscribe prefix MY_MQTT_SUBSCRIBE_TOPIC_PREFIX for this MQTT client
#endif

#if !defined(MY_MQTT_CLIENT_ID)
#error You must define a unique MY_MQTT_CLIENT_ID for this MQTT client
#endif

#include "core/MyGatewayTransport.h"
#include "core/MyProtocolMySensors.h"

#if defined(MY_GATEWAY_TINYGSM)
#include "drivers/TinyGSM/TinyGsmClient.h"
#endif

#if defined(MY_GATEWAY_LINUX)
#include "drivers/Linux/EthernetClient.h"
#include "drivers/Linux/EthernetServer.h"
#include "drivers/Linux/IPAddress.h"
#endif
#include "drivers/PubSubClient/PubSubClient.h"
#include "core/MyGatewayTransportMQTTClient.h"
#elif defined(MY_GATEWAY_FEATURE)
// GATEWAY - COMMON FUNCTIONS
#include "MySensors/core/MyGatewayTransport.h"
#include "MySensors/core/MyProtocol.h"
#include "MySensors/core/MySensorsCore.h"

//#include "MySensors/core/MyProtocolMySensors.cpp"

// GATEWAY - CONFIGURATION
#if defined(MY_SENSOR_NETWORK)
// We assume that a gateway having a radio also should act as repeater
#define MY_REPEATER_FEATURE
#endif

#if !defined(MY_PORT)
#error You must define MY_PORT (controller or gateway port to open)
#endif
#if defined(MY_GATEWAY_ESP8266) || defined(MY_GATEWAY_ESP32)
// GATEWAY - ESP8266 / ESP32
#include "core/MyGatewayTransportEthernet.h"
#elif defined(MY_GATEWAY_LINUX)
// GATEWAY - Generic Linux
#if defined(MY_USE_UDP)
#error UDP mode is not available for Linux
#endif
#include "drivers/Linux/EthernetClient.h"
#include "drivers/Linux/EthernetServer.h"
#include "drivers/Linux/IPAddress.h"
#include "core/MyGatewayTransportEthernet.h"
#elif defined(MY_GATEWAY_W5100)
// GATEWAY - W5100
#include "core/MyGatewayTransportEthernet.h"
#elif defined(MY_GATEWAY_ENC28J60)
// GATEWAY - ENC28J60
#if defined(MY_USE_UDP)
#error UDP mode is not available for ENC28J60
#endif
#include "core/MyGatewayTransportEthernet.h"
#elif defined(MY_GATEWAY_SERIAL)
// GATEWAY - SERIAL
//#include "core/MyGatewayTransport.h"
#endif
#endif

// TRANSPORT
#ifndef DOXYGEN
// count enabled transports
#if defined(MY_RADIO_RF24)
#define __RF24CNT 1		//!< __RF24CNT
#else
#define __RF24CNT 0		//!< __RF24CNT
#endif
#if defined(MY_RADIO_NRF5_ESB)
#define __NRF5ESBCNT 1 //!< __NRF5ESBCNT
#else
#define __NRF5ESBCNT 0 //!< __NRF5ESBCNT
#endif
#if defined(MY_RADIO_RFM69)
#define __RFM69CNT 1	//!< __RFM69CNT
#else
#define __RFM69CNT 0	//!< __RFM69CNT
#endif
#if defined(MY_RADIO_RFM95)
#define __RFM95CNT 1	//!< __RFM95CNT
#else
#define __RFM95CNT 0	//!< __RFM95CNT
#endif
#if defined(MY_RS485)
#define __RS485CNT 1	//!< __RS485CNT
#else
#define __RS485CNT 0	//!< __RS485CNT
#endif

#if (__RF24CNT + __NRF5ESBCNT + __RFM69CNT + __RFM95CNT + __RS485CNT > 1)
#error Only one forward link driver can be activated
#endif
#endif //DOXYGEN

// SANITY CHECK
#if defined(MY_REPEATER_FEATURE) || defined(MY_GATEWAY_FEATURE)
#define MY_TRANSPORT_SANITY_CHECK		//!< enable regular transport sanity checks
#endif

// TRANSPORT INCLUDES
#if defined(MY_RADIO_RF24) || defined(MY_RS485) || defined(MY_RS232)
//#include "hal/transport/MyTransportHAL.h"
#include "core/MyTransport.h"

// PARENT CHECK
#if defined(MY_PARENT_NODE_IS_STATIC) && (MY_PARENT_NODE_ID == AUTO)
#error Parent is static but no parent ID defined, set MY_PARENT_NODE_ID.
#endif

#if defined(MY_TRANSPORT_DONT_CARE_MODE)
#error MY_TRANSPORT_DONT_CARE_MODE is deprecated, set MY_TRANSPORT_WAIT_READY_MS instead!
#endif

// RAM ROUTING TABLE
#if defined(MY_RAM_ROUTING_TABLE_FEATURE) && defined(MY_REPEATER_FEATURE)
// activate feature based on architecture
#if defined(ARDUINO_ARCH_ESP8266) || defined(ARDUINO_ARCH_ESP32) || defined(ARDUINO_ARCH_SAMD) || defined(ARDUINO_ARCH_NRF5) || defined(ARDUINO_ARCH_STM32F1) || defined(TEENSYDUINO) || defined(__linux__)
#define MY_RAM_ROUTING_TABLE_ENABLED
#elif defined(ARDUINO_ARCH_AVR)
#if defined(__avr_atmega1280__) || defined(__avr_atmega1284__) || defined(__avr_atmega2560__)
// >4kb, enable it
#define MY_RAM_ROUTING_TABLE_ENABLED
#else
// memory limited, enable with care
// #define MY_RAM_ROUTING_TABLE_ENABLED
#endif // __avr_atmega1280__, __avr_atmega1284__, __avr_atmega2560__
#endif // ARDUINO_ARCH_AVR
#endif

#ifdef DOXYGEN
/**
 * @def MY_RAM_ROUTING_TABLE_ENABLED
 * @brief Automatically set if RAM routing table is enabled
 *
 * @see MY_RAM_ROUTING_TABLE_FEATURE
 */
#define MY_RAM_ROUTING_TABLE_ENABLED
#endif

// SOFTSPI
#ifdef MY_SOFTSPI
#if defined(ARDUINO_ARCH_ESP8266)
#error Soft SPI is not available on ESP8266
#endif
#include "hal/architecture/AVR/drivers/DigitalIO/DigitalIO.h"
#endif

// SOFTSERIAL
#if defined(MY_GSM_TX) != defined(MY_GSM_RX)
#error Both, MY_GSM_TX and MY_GSM_RX need to be defined when using SoftSerial
#endif

#if defined(MY_GATEWAY_TINYGSM) && !defined(SerialAT) && (!defined(MY_GSM_TX) || !defined(MY_GSM_RX))
#error You need to define either SerialAT or MY_GSM_RX and MY_GSM_TX pins
#endif

// POWER PIN

#define RADIO_CAN_POWER_OFF (false)


// Transport drivers
#if defined(MY_RADIO_RF24)
//    #include "MySensors/hal/transport/RF24/driver/RF24.h"
//    #include "MySensors/hal/transport/RF24/MyTransportRF24.cpp"
    #include "MySensors/hal/transport/RF24/MyTransportRF24.h"
#elif defined(MY_RS485)
    #include "hal/transport/RS485/MyTransportRS485.cpp"
#elif defined(MY_RS232)
    #include "hal/transport/RS232/MyTransportRS232.cpp"
#endif

// PASSIVE MODE
#if defined(MY_PASSIVE_NODE) && !defined(DOXYGEN)
#define MY_TRANSPORT_UPLINK_CHECK_DISABLED
#define MY_PARENT_NODE_IS_STATIC
#undef MY_REGISTRATION_FEATURE
#undef MY_SIGNING_FEATURE
#undef MY_OTA_FIRMWARE_FEATURE
#if (defined(MY_GATEWAY_FEATURE) || defined(MY_REPEATER_FEATURE))
#error This node is configured as GW/repeater, MY_PASSIVE_NODE cannot be set simultaneously
#endif
#if (MY_NODE_ID == AUTO)
#error MY_PASSIVE_NODE configuration requires setting MY_NODE_ID
#endif
#endif

#include "MySensors/core/MyTransport.h"
#endif

// Make sure to disable child features when parent feature is disabled
#if !defined(MY_SENSOR_NETWORK)
#undef MY_OTA_FIRMWARE_FEATURE
#undef MY_REPEATER_FEATURE
#undef MY_SIGNING_NODE_WHITELISTING
#undef MY_SIGNING_FEATURE
#endif

#if !defined(MY_GATEWAY_FEATURE)
#undef MY_INCLUSION_MODE_FEATURE
#undef MY_INCLUSION_BUTTON_FEATURE
#endif

#if !defined(MY_CORE_ONLY)
#if !defined(MY_GATEWAY_FEATURE) && !defined(MY_SENSOR_NETWORK)
#error No forward link or gateway feature activated. This means nowhere to send messages! Pretty pointless.
#endif
#endif

#include "core/MyCapabilities.h"
#include "core/MyMessage.h"
#include "core/MySplashScreen.h"
#include "core/MySensorsCore.h"

// HW mains
#if defined(ARDUINO_ARCH_AVR)
#include "hal/architecture/AVR/MyMainAVR.h"
#elif defined(ARDUINO_ARCH_SAMD)
#include "hal/architecture/SAMD/MyMainSAMD.h"
#elif defined(ARDUINO_ARCH_ESP8266)
#include "hal/architecture/ESP8266/MyMainESP8266.h"
#elif defined(ARDUINO_ARCH_NRF5)
#include "hal/architecture/NRF5/MyMainNRF5.h"
#elif defined(ARDUINO_ARCH_ESP32)
#include "hal/architecture/ESP32/MyMainESP32.h"
#elif defined(__linux__)
#include "hal/architecture/Linux/MyMainLinuxGeneric.h"
#elif defined(ARDUINO_ARCH_STM32F1)
#include "hal/architecture/STM32F1/MyMainSTM32F1.h"
#elif defined(TEENSYDUINO)
#include "hal/architecture/Teensy3/MyMainTeensy3.h"
#elif defined(__MSP430G2553__)
    #include "hal/architecture/MyHwMSP430.h"
#endif

#endif

/** @}*/
