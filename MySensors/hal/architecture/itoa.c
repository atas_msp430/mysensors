/*
 * itoa.c
 *
 *  Created on: 16 ����. 2019 �.
 *      Author: Atas
 */

#include "itoa.h"

static divmod10_t divmodu10(unsigned long n)
{
    divmod10_t res;
// �������� �� 0.8
    res.quot = n >> 1;
    res.quot += res.quot >> 1;
    res.quot += res.quot >> 4;
    res.quot += res.quot >> 8;
    res.quot += res.quot >> 16;
    unsigned long qq = res.quot;
// ����� �� 8
    res.quot >>= 3;
// ��������� �������
//    res.rem = uint8_t(n - ((res.quot << 1) + (qq & ~7ul)));
    res.rem = (unsigned char)(n - ((res.quot << 1) + (qq & ~7ul)));
// ������������ ������� � �������
    if(res.rem > 9)
    {
        res.rem -= 10;
        res.quot++;
    }
    return res;
}

char * utoa(unsigned long value, char *buffer)
{
    buffer += 11;
    *--buffer = 0;
    do
    {
        divmod10_t res = divmodu10(value);
        *--buffer = res.rem + '0';
        value = res.quot;
    }
    while (value != 0);
    return buffer;
}


char * itoa(long value, char *buffer)
{
    bool isNegative = (value<0);
    if (isNegative) value = - value;

    buffer = utoa(value, buffer);
    buffer += 11;
    *--buffer = 0;
    do
    {
        divmod10_t res = divmodu10(value);
        *--buffer = res.rem + '0';
        value = res.quot;
    }
    while (value != 0);
    if (isNegative) *--buffer = '-';
    return buffer;
}

