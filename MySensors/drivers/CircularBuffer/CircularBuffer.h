﻿/*
  CircularBuffer - An Arduino circular buffering library for arbitrary types.

  Created by Ivo Pullens, Emmission, 2014-2016 -- www.emmission.nl

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
* @file CircularBuffer.h
*
* Circular buffering for arbitrary types.
*/

#ifndef CircularBuffer_h
#define CircularBuffer_h

/**
 * The circular buffer class.
 * Pass the datatype to be stored in the buffer as template parameter.
 */
template <class T> class CircularBuffer
{
public:
	/**
	 * Constructor
	 * @param buffer   Preallocated buffer of at least size records.
	 * @param size     Number of records available in the buffer.
	 */
    CircularBuffer(T* buffer, const unsigned char size ) : m_size(size), m_buff(buffer) , _mask(size-1) //m_size - value MUST be power of two (2,4,8,16,32...)
	{
		clear();
	}

	/**
	  * Clear all entries in the circular buffer.
	  */
	void clear(void)
	{
			m_front = 0;
			m_fill  = 0;
	}

	/**
	 * Test if the circular buffer is empty.
	 * @return True, when empty.
	 */
	inline bool empty(void) const
	{
		bool empty;
			empty = !m_fill;
		return empty;
	}

	/**
	 * Test if the circular buffer is full.
	 * @return True, when full.
	 */
	inline bool full(void) const
	{
		return m_fill == m_size;
	}

	/**
	 * Return the number of records stored in the buffer.
	 * @return number of records.
	 */
	inline unsigned char available(void) const
	{
		return m_fill;
	}

	/**
	 * Aquire unused record on front of the buffer, for writing.
	 * After filling the record, it has to be pushed to actually
	 * add it to the buffer.
	 * @return Pointer to record, or NULL when buffer is full.
	 */
	T* getFront(void) const
	{
			if (!full())
			{
				return &m_buff[m_front];
			}
		return static_cast<T*>(nullptr);
	}

	/**
	 * Push record to front of the buffer.
	 * @param record   Record to push. If record was aquired previously (using getFront) its
	 *                 data will not be copied as it is already present in the buffer.
	 * @return True, when record was pushed successfully.
	 */
	bool pushFront(T * record)
	{
	    if (full())	        return false;

	    T* f =&(m_buff[m_front]);
	    if (f != record) {
	        *f = *record;
	    }
	    m_front = (m_front+1)& _mask;
	    m_fill++;
	    return true;
	}

	/**
	 * Aquire record on back of the buffer, for reading.
	 * After reading the record, it has to be pop'ed to actually
	 * remove it from the buffer.
	 * @return Pointer to record, or NULL when buffer is empty.
	 */
	T* getBack(void) const
	{
	    if (empty())    return static_cast<T*>(nullptr);
	    else 		    return &(m_buff[back()]);
	}

	/**
	 * Remove record from back of the buffer.
	 * @return True, when record was pop'ed successfully.
	 */
	bool popBack(void)
	{
	    if (empty())    return false;

	    m_fill--;
	    return true;
	}
protected:

	/**
	 * Internal getter for index of last used record in buffer.
	 * @return Index of last record.
	 */
	inline unsigned char back(void) const
	{
		return (m_front - m_fill + m_size) & _mask;
	}

	const unsigned char      m_size;     //!< Total number of records that can be stored in the buffer.
	T* const                 m_buff;     //!< Ptr to buffer holding all records.
	volatile unsigned char   m_front;    //!< Index of front element (not pushed yet).
	volatile unsigned char   m_fill;     //!< Amount of records currently pushed.
private:
    // mask for index which prevents overflow. if size=4 what corresponds to b0100 then mask=size-1 what corresponds to b0011
            const unsigned char _mask = m_size - 1;

};

#endif // CircularBuffer_h
