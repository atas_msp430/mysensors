#ifdef __cplusplus
extern "C" {
#endif
#include "soft_printf.h"

//#include "serial.h"

extern void putc(unsigned c);
//extern void puts(const unsigned char *s);
extern void puts(const char *_ptr);

static const char _tru[] = "TRUE";
static const char _fals[]= "FALSE";


static const unsigned long dv[] = {
//  4294967296      // 32 bit unsigned max
    1000000000,     // +0
     100000000,     // +1
      10000000,     // +2
       1000000,     // +3
        100000,     // +4
//       65535      // 16 bit unsigned max     
         10000,     // +5
          1000,     // +6
           100,     // +7
            10,     // +8
             1,     // +9
};


const unsigned char hexChar[] = "0123456789ABCDEF";

static const unsigned long hv[] = {0x10000000, 0x1000000, 0x100000, 0x10000, 0x1000, 0x100, 0x10, 1, 0};

static void xtoa(unsigned long x, const unsigned long *dp)
{
    char c;
    unsigned long d;
    if(x) {
        while(x < *dp) ++dp;
        do {
            d = *dp++;
            c = '0';
            while(x >= d) ++c, x -= d;
            putc(c);
        } while(!(d & 1));
    } else
        putc('0');
}

static void puth(unsigned n)
{

    putc(hex(n));
}

void soft_printf(const char * format, ...)
{
    char c;
    int i;
    long n;
    
    va_list a;
    va_start(a, format);
    while(c = *format++) {
        if(c == '%') {
            switch(c = *format++) {
                case 's':                       // String
                    puts(va_arg(a, char*));
                    break;
                case 'c':                       // Char
                    putc(va_arg(a, char));
                    break;
                case 'i':                       // 16 bit Integer
                case 'u':                       // 16 bit Unsigned
                    i = va_arg(a, int);
                    if(c == 'i' && i < 0) i = -i, putc('-');
                    xtoa((unsigned)i, dv + 5);
                    break;
                case 'l':                       // 32 bit Long
                case 'n':                       // 32 bit uNsigned loNg
                    n = va_arg(a, long);
                    if(c == 'l' &&  n < 0) n = -n, putc('-');
                    xtoa((unsigned long)n, dv);
                    break;
                case 'x':                       // 16 bit heXadecimal
                case 'h':                       // 8 bit heXadecimal
                case 'H':                       // 16 bit HEX
                    i = va_arg(a, int);
                    if ((c=='x') || (c=='H') || (i>0xFF)){
                        puth(i >> 12);
                        puth(i >> 8);
                    }
					puth(i >> 4);
					puth(i);
					break;
                case 'b':                       // 8 bit bin
					i = va_arg(a, char);
					char j;
					for (j=8; j>0;j--){
						putc('0'+ ((i & 0x80)>>7));

						i =i << 1;
					}
					break;
                case '?':                       // bool
                    i = va_arg(a, char);
                    if (i) puts(_tru);
                    else puts(_fals);
                            break;

                case 0: return;
                default: goto bad_fmt;
            }
        } else
bad_fmt:    putc(c);
    }
    va_end(a);
}

static char * xtoa_P(char * const bufForString, unsigned long x, const unsigned long *dp)
{
    char c;
    char *s =bufForString;
    unsigned long d;
    if(x) {
        while(x < *dp) ++dp;
        do {
            d = *dp++;
            c = '0';
            while(x >= d) ++c, x -= d;
            *s++=c;
        } while(!(d & 1));
    } else
        *s++='0';
    return s;
}

void snprintf_P(char * const stringBuf,
//                unsigned int sizeN,
                const char *format, ...)
{
    char c;
    int i;
    long n;
    char* strBuf = stringBuf;
//    char* endBuf = s+sizeN;
    char* str_t;
    va_list a;
    va_start(a, format);
    while(c = *format++) {
        if(c == '%') {
            switch(c = *format++) {
                case 's':                       // String
                    str_t = va_arg(a, char*);
                    while(*str_t) *strBuf++=*str_t++;//puts(va_arg(a, char*));
                    break;
                case 'c':                       // Char
                    *strBuf++=va_arg(a, char);//putc(va_arg(a, char));
                    break;
                case 'i':                       // 16 bit Integer
                case 'u':                       // 16 bit Unsigned
                    i = va_arg(a, int);
                    if(c == 'i' && i < 0) i = -i, *strBuf++='-';//putc('-');
                    strBuf = xtoa_P(strBuf, (unsigned)i, dv + 5);
                    break;
                case 'l':                       // 32 bit Long
                case 'n':                       // 32 bit uNsigned loNg
                    n = va_arg(a, long);
                    if(c == 'l' &&  n < 0) n = -n, *strBuf++='-';
                    strBuf = xtoa_P(strBuf, (unsigned long)n, dv);
                    break;
                case 'h':                       // 8 bit heXadecimal
                case 'H':                       // 16 bit HEX
                    i = va_arg(a, int);
                    if ((c=='H') || (i>0xFF)){
                        *strBuf++= hex(i >> 12);
                        *strBuf++= hex(i >> 8);
                    }
                    *strBuf++= hex(i >> 4);
                    *strBuf++= hex(i);
                    break;
                case 'b':                       // 8 bit bin
                    i = va_arg(a, char);
                    char j;
                    for (j=8; j>0;j--){
                        *strBuf++=('0'+ ((i & 0x80)>>7));//putc('0'+ ((i & 0x80)>>7));

                        i =i << 1;
                    }
                    break;
                case 0: return;
                default: goto bad_fmt;
            }
        } else
bad_fmt:    *strBuf++=c;//putc(c);
    }
    va_end(a);

*strBuf=0x00;
}


/* this only works for base==10 or 16 */
unsigned char* strAtoI(unsigned char * str, unsigned long value, unsigned char base) {
    unsigned long d;
    unsigned int c;
    const unsigned long *dp;
    unsigned char *s = str;

    if (base==16) {
        *str++ = '0'; *str++ = 'x';
        dp = hv;    // start at the top of the array
    } else
        dp = dv; // select the base 10 array
    while (value < *dp) // move to the correct decade
        dp++;
    do {
        d = *dp++;
        c = 0;    // count binary
        while((value >= d) && (d!=0)) ++c, value -= d;
        *str++ = hex(c);   //convert to ASCII
    } while(d>1);
    *str++ = 0;
    return s;
}



#ifdef __cplusplus
}
#endif

